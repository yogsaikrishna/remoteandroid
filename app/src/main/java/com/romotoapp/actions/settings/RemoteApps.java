package com.romotoapp.actions.settings;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerTabStrip;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;

import com.romotoapp.BaseActivity;
import com.romotoapp.R;

public class RemoteApps extends BaseActivity {
	private static String[] titles = {"DOWNLOADED", "RUNNING", "ALL"};
	private PagerAdapter pagerAdapter;
	private PagerTabStrip pagerTabStrip;
	private ViewPager viewPager;
	private Bundle bundle;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		bundle = getIntent().getExtras();
		setContentView(R.layout.tab_strip_layout);
		pagerAdapter = new PagerAdapter(getSupportFragmentManager(), bundle);
		final ActionBar actionBar = getSupportActionBar();
		actionBar.setHomeButtonEnabled(false);
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		pagerTabStrip = (PagerTabStrip) findViewById(R.id.pager_title_strip);
		viewPager = (ViewPager) findViewById(R.id.pager);
		viewPager.setAdapter(pagerAdapter);
		pagerTabStrip.setTabIndicatorColorResource(R.color.color);
	}
	
	public static class PagerAdapter extends FragmentPagerAdapter {
		private Bundle bundle;
		
		public PagerAdapter(FragmentManager fm, Bundle bundle) {
			super(fm);
			this.bundle = bundle;
		}

		@Override
		public Fragment getItem(int i) {
			Fragment fragment;
			switch (i) {
			case 0:
				fragment = new DownloadedAppsFragment();
				fragment.setArguments(bundle);
				return fragment;
			case 1:
				fragment = new RunningAppsFragment();
				fragment.setArguments(bundle);
				return fragment;
			case 2:
				fragment = new AllAppsFragment();
				fragment.setArguments(bundle);
				return fragment;
			default:
				fragment = new DownloadedAppsFragment();
				fragment.setArguments(bundle);
				return fragment;
			}
		}

		@Override
		public int getCount() {
			return 3;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			return titles[position];
		}
		
	}
}
