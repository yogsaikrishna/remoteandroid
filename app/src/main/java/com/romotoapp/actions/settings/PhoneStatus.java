package com.romotoapp.actions.settings;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.romotoapp.BaseActivity;
import com.romotoapp.R;
import com.romotoapp.common.ListAdapter;
import com.romotoapp.common.RemoteSettings;
import com.romotoapp.common.RemoteSettings.Status;
import com.romotoapp.common.RowData;

public class PhoneStatus extends BaseActivity implements OnItemClickListener {
	private RemoteSettings remoteSettings;
	private ListView listView;
	private TextView tvListDesc;
	private Vector<RowData> data;
	private RowData rowData;
	private ListAdapter listAdapter;
	private String[] primaryText = {"SIM status", "Wi-Fi MAC address", "Serial number"};
	private List<String> secondaryText;
	private Status status;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		remoteSettings = getIntent().getExtras().getParcelable("remote_settings");
		setContentView(R.layout.list_layout);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setTitle("Status");
		
		status = remoteSettings.getAbout().getStatus();
		data = new Vector<RowData>();
		secondaryText = new ArrayList<String>();
		secondaryText.add("SIM-related information");
		secondaryText.add(status.getWifiMac());
		secondaryText.add(status.getSerialNumber());
		
		listView = (ListView) findViewById(R.id.listView);
		tvListDesc = (TextView) findViewById(R.id.tv_list_desc);
		tvListDesc.setVisibility(View.GONE);
		
		for (int count = 0; count < primaryText.length; count++) {
			rowData = new RowData(primaryText[count], secondaryText.get(count), 
					R.drawable.ic_action_phone, false);
			data.add(rowData);
		}
		
		listAdapter = new ListAdapter(this, R.layout.device_list_item,
				R.id.tv_text, data);
		listView.setAdapter(listAdapter);
		listView.setOnItemClickListener(this);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			this.finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		switch (position) {
		case 0:
			Intent intent = new Intent(PhoneStatus.this, SIMStatus.class);
			intent.putExtra("remote_settings", remoteSettings);
			startActivity(intent);
			break;
		}
	}
}
