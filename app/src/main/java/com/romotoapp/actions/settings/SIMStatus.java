package com.romotoapp.actions.settings;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.romotoapp.BaseActivity;
import com.romotoapp.R;
import com.romotoapp.common.ListAdapter;
import com.romotoapp.common.RemoteSettings;
import com.romotoapp.common.RemoteSettings.SIM;
import com.romotoapp.common.RowData;

public class SIMStatus extends BaseActivity {
	private RemoteSettings remoteSettings;
	private ListView listView;
	private TextView tvListDesc;
	private Vector<RowData> data;
	private RowData rowData;
	private ListAdapter listAdapter;
	private String[] primaryText = {"Network", "Mobile network type", "Service status",
			"Roaming", "Mobile network state", "IMEI", "IMEI SV"};
	private List<String> secondaryText;
	private SIM sim;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		remoteSettings = getIntent().getExtras().getParcelable("remote_settings");
		setContentView(R.layout.list_layout);
		
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setTitle("SIM status");
		
		sim = remoteSettings.getAbout().getStatus().getSim();
		data = new Vector<RowData>();
		secondaryText = new ArrayList<String>();
		secondaryText.add(sim.getNetwork());
		secondaryText.add(sim.getMobileNetworkType());
		secondaryText.add(sim.getServiceStatus());
		secondaryText.add(sim.getRoaming());
		secondaryText.add(sim.getMobileNetworkState());
		secondaryText.add(sim.getImei());
		secondaryText.add(sim.getImeiSv());
		
		listView = (ListView) findViewById(R.id.listView);
		tvListDesc = (TextView) findViewById(R.id.tv_list_desc);
		tvListDesc.setVisibility(View.GONE);
		
		for (int count = 0; count < primaryText.length; count++) {
			rowData = new RowData(primaryText[count], secondaryText.get(count), 
					R.drawable.ic_action_phone, false);
			data.add(rowData);
		}
		
		listAdapter = new ListAdapter(this, R.layout.device_list_item,
				R.id.tv_text, data);
		listView.setAdapter(listAdapter);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			this.finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
