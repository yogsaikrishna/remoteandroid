package com.romotoapp.actions.settings;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.romotoapp.BaseActivity;
import com.romotoapp.R;
import com.romotoapp.common.RemoteSettings.Application;
import com.romotoapp.common.RemoteSettings.Permissions;
import com.romotoapp.constants.Constants;
import com.romotoapp.utils.Utils;

public class RemoteAppInfo extends BaseActivity {
	private TextView appName;
	private TextView appVersion;
	private TextView totalSpace;
	private TextView appSpace;
	private TextView dataSpace;
	private TextView cacheSpace;
	private ImageView appIcon;
	private Application app;
	private Bitmap bitmap;
	private LinearLayout appInfoContainer;
	private LinearLayout.LayoutParams layoutParams;
	private Map<String, List<String>> permissions;
	private List<Permissions> appPermissions;
	private StringBuffer strBuffer;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		app = getIntent().getExtras().getParcelable("remote_app");
		appPermissions = app.getPermissions();
		bitmap = getIntent().getExtras().getParcelable("app_icon");
		setContentView(R.layout.remote_app_info);
		
		permissions = new HashMap<String, List<String>>();
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setTitle("App info");
		
		appIcon = (ImageView) findViewById(R.id.iv_app_icon);
		appName = (TextView) findViewById(R.id.tv_app_name);
		appVersion = (TextView) findViewById(R.id.tv_app_version);
		totalSpace = (TextView) findViewById(R.id.tv_total_space);
		appSpace = (TextView) findViewById(R.id.tv_app_space);
		dataSpace = (TextView) findViewById(R.id.tv_data_space);
		cacheSpace = (TextView) findViewById(R.id.tv_cache_space);
		appInfoContainer = (LinearLayout) findViewById(R.id.app_info_container);
		
    	int size = Utils.getDisplaySize(this, Constants.DENSITY_SCALED);
		layoutParams = new LinearLayout.LayoutParams(size, size);
		appIcon.setLayoutParams(layoutParams);
		
		if (bitmap != null) {
			appIcon.setImageBitmap(bitmap);
		}
		appName.setText(app.getName());
		appVersion.setText("version " + app.getVersion());
		totalSpace.setText(convertedSize(app.getTotalSize()));
		appSpace.setText(convertedSize(app.getAppSize()));
		dataSpace.setText(convertedSize(app.getData()));
		cacheSpace.setText(convertedSize(app.getCache()));
		
		for (Permissions perm : appPermissions) {
			if (perm != null) {
				String type = perm.getType();
				if (!permissions.containsKey(type)) {
					permissions.put(type, getDescriptionList(type));
				}
			}
		}
		
		Iterator<Entry<String, List<String>>> iterator = permissions.entrySet().iterator();
		while (iterator.hasNext()) {
			strBuffer = new StringBuffer();
			Entry<String, List<String>> entry = iterator.next();
			String type = entry.getKey();
			List<String> tmpList = entry.getValue();
			int listSize = tmpList.size();
			for (int count = 0; count < listSize - 1; count++) {
				strBuffer.append(tmpList.get(count) + ", ");
			}
			strBuffer.append(tmpList.get(listSize - 1));
			String description = strBuffer.toString();
			if (type == null || type.equals("")
				|| description == null || description.equals("")) {
				continue;
			}
			LinearLayout linearLayout = new LinearLayout(this);
			LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
					LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			linearLayout.setLayoutParams(layoutParams);
			linearLayout.setOrientation(LinearLayout.VERTICAL);
			layoutParams.setMargins(25, 15, 25, 15);
			TextView permType = new TextView(this);
			permType.setText(type);
			permType.setTextSize(16);
			permType.setTypeface(null, Typeface.BOLD);
			linearLayout.addView(permType);
			TextView permDescription = new TextView(this);
			permDescription.setText(description);
			permDescription.setTextSize(15);
			linearLayout.addView(permDescription);
			appInfoContainer.addView(linearLayout);
		}
	}
	
	private String convertedSize(double size) {
		String tmpSize = null;
		if (size > Constants.MB) {
			size /= Constants.MB;
			size = Math.round(size * 100.0) / 100.0;
			tmpSize = size + "MB";
		} else {
			size /= Constants.KB;
			size = Math.round(size * 100.0) / 100.0;
			tmpSize = size + "KB";			
		}
		return tmpSize;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			this.finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private List<String> getDescriptionList(String type) {
		List<String> tmpList = new ArrayList<String>();
		for (Permissions perm : appPermissions) {
			if (perm != null) {
				String permType = perm.getType();
				String description = perm.getDescription();
				if (type.equals(permType)) {
					tmpList.add(description);
				}
			}
		}
		return tmpList;
	}
}
