package com.romotoapp.actions.settings;

import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

import com.romotoapp.R;
import com.romotoapp.common.ListAdapter;
import com.romotoapp.common.RemoteSettings;
import com.romotoapp.common.RowData;
import com.romotoapp.common.RemoteSettings.Application;
import com.romotoapp.constants.Constants;
import com.romotoapp.constants.SessionConstants;
import com.romotoapp.interfaces.OnFileDownloadListener;
import com.romotoapp.utils.FileUtils;

public class AllAppsFragment extends Fragment implements
		OnItemClickListener, OnFileDownloadListener {
	private RemoteSettings remoteSettings;
	private ListView listView;
	private TextView tvListDesc;
	private Vector<RowData> data;
	private RowData rowData;
	private ListAdapter listAdapter;
	private List<Application> apps;
	private Bundle bundle;
	private String[] fileNames;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		bundle = getArguments();
		remoteSettings = bundle.getParcelable("remote_settings");
		apps = remoteSettings.getApps().getAll();
		data = new Vector<RowData>();
		fileNames = new String[apps.size()];

		View view = inflater.inflate(R.layout.list_layout, container, false);
		listView = (ListView) view.findViewById(R.id.listView);
		tvListDesc = (TextView) view.findViewById(R.id.tv_list_desc);
		tvListDesc.setVisibility(View.GONE);

		if (SessionConstants.allAppIcons == null) {
			int appCount = 0;
			Iterator<Application> appIterator = apps.iterator();
			while (appIterator.hasNext()) {
				Application tmpApp = appIterator.next();
				if (tmpApp != null) {
					fileNames[appCount++] = tmpApp.getIconUrl();
					double totalSize = tmpApp.getTotalSize() / Constants.MB;
					rowData = new RowData(tmpApp.getName(), ""
							+ Math.round(totalSize * 100.0) / 100.0 + "MB",
							R.drawable.ic_android_app, true);
					data.add(rowData);
				}
			}
			FileUtils.downloadFile(this, fileNames);
		} else {
			buildAppList();
		}

		listAdapter = new ListAdapter(getActivity(), R.layout.device_list_item,
				R.id.tv_text, data);
		listView.setAdapter(listAdapter);
		listView.setOnItemClickListener(this);
		return view;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		Intent intent = new Intent(getActivity(), RemoteAppInfo.class);
		intent.putExtra("remote_app", apps.get(position));
		if (SessionConstants.allAppIcons != null) {
			intent.putExtra("app_icon", SessionConstants.allAppIcons.get(position));
		}
		startActivity(intent);
	}

	@Override
	public void onFileDownload(String jsonString) {

	}

	@Override
	public void onFileDownload(List<Bitmap> bitmaps) {
		SessionConstants.allAppIcons = bitmaps;
		buildAppList();
		listAdapter.notifyDataSetChanged();
	}

	private void buildAppList() {
		if (SessionConstants.allAppIcons != null) {
			List<Bitmap> bitmaps = SessionConstants.allAppIcons;
			data.clear();
			for (int count = 0; count < apps.size(); count++) {
				Application tmpApp = apps.get(count);
				if (tmpApp != null) {
					double totalSize = tmpApp.getTotalSize() / Constants.MB;
					rowData = new RowData(tmpApp.getName(), ""
							+ Math.round(totalSize * 100.0) / 100.0 + "MB",
							bitmaps.get(count), true);
					data.add(rowData);
				}
			}
		}
	}
}
