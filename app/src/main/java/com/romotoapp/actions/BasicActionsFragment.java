package com.romotoapp.actions;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Switch;
import android.widget.ToggleButton;

import com.google.gson.Gson;
import com.romotoapp.R;
import com.romotoapp.actions.settings.AboutPhone;
import com.romotoapp.actions.settings.RemoteApps;
import com.romotoapp.common.RemoteSettings;
import com.romotoapp.utils.Utils;

public class BasicActionsFragment extends Fragment implements OnClickListener {
	private Gson gson;
	private String json;
	private RemoteSettings remoteSettings;
	private FrameLayout aboutPhone;
	private FrameLayout apps;
	private Switch mWifi;
	private Switch mBluetooth;
	private Switch mFlightMode;
	private ToggleButton sWifi;
	private ToggleButton sBluetooth;
	private ToggleButton sFlightMode;
	private int buildVersion;
	
	@SuppressLint("NewApi")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Bundle bundle = getArguments();
		gson = new Gson();
		if (bundle != null) {
			json = bundle.getString("json");
			if (json != null) {
				remoteSettings = gson.fromJson(json, RemoteSettings.class);
			}
		}
		View view = inflater.inflate(R.layout.remote_basic, container, false);
		apps = (FrameLayout) view.findViewById(R.id.apps);
		aboutPhone = (FrameLayout) view.findViewById(R.id.about_phone);
		apps.setOnClickListener(this);
		aboutPhone.setOnClickListener(this);
		buildVersion = Utils.buildVersion();
		if (buildVersion < 14) {
			sWifi = (ToggleButton) view.findViewById(R.id.sw_wifi);
			sBluetooth = (ToggleButton) view.findViewById(R.id.sw_bluetooth);
			sFlightMode = (ToggleButton) view.findViewById(R.id.sw_airplane);
			
			sWifi.setChecked(remoteSettings.getWifi().isStatus());
			sBluetooth.setChecked(remoteSettings.getBluetooth().isStatus());
			sFlightMode.setChecked(remoteSettings.getFlight().isStatus());
		} else {
			mWifi = (Switch) view.findViewById(R.id.sw_wifi);
			mBluetooth = (Switch) view.findViewById(R.id.sw_bluetooth);
			mFlightMode = (Switch) view.findViewById(R.id.sw_airplane);

			mWifi.setChecked(remoteSettings.getWifi().isStatus());
			mBluetooth.setChecked(remoteSettings.getBluetooth().isStatus());
			mFlightMode.setChecked(remoteSettings.getFlight().isStatus());
		}
		
		return view;
	}

	@Override
	public void onClick(View v) {
		Intent intent = new Intent();
		intent.putExtra("remote_settings", remoteSettings);
		switch (v.getId()) {
		case R.id.apps:
			intent.setClass(getActivity(), RemoteApps.class);
			break;
		case R.id.about_phone:
			intent.setClass(getActivity(), AboutPhone.class);
			break;
		}
		startActivity(intent);
	}
}
