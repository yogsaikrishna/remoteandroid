package com.romotoapp.services;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.IPackageStatsObserver;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.PackageStats;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiConfiguration.KeyMgmt;
import android.net.wifi.WifiConfiguration.Protocol;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.RemoteException;
import android.provider.Settings;
import android.provider.Settings.SettingNotFoundException;
import android.telephony.ServiceState;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.google.gson.Gson;
import com.romotoapp.common.RemoteSettings;
import com.romotoapp.common.RemoteSettings.About;
import com.romotoapp.common.RemoteSettings.Application;
import com.romotoapp.common.RemoteSettings.Apps;
import com.romotoapp.common.RemoteSettings.AvailableNetworks;
import com.romotoapp.common.RemoteSettings.Battery;
import com.romotoapp.common.RemoteSettings.Bluetooth;
import com.romotoapp.common.RemoteSettings.Brightness;
import com.romotoapp.common.RemoteSettings.DateTime;
import com.romotoapp.common.RemoteSettings.Display;
import com.romotoapp.common.RemoteSettings.Flight;
import com.romotoapp.common.RemoteSettings.Memory;
import com.romotoapp.common.RemoteSettings.PairedDevices;
import com.romotoapp.common.RemoteSettings.Permissions;
import com.romotoapp.common.RemoteSettings.Process;
import com.romotoapp.common.RemoteSettings.SIM;
import com.romotoapp.common.RemoteSettings.Service;
import com.romotoapp.common.RemoteSettings.Sound;
import com.romotoapp.common.RemoteSettings.Stats;
import com.romotoapp.common.RemoteSettings.Status;
import com.romotoapp.common.RemoteSettings.Storage;
import com.romotoapp.common.RemoteSettings.Volume;
import com.romotoapp.common.RemoteSettings.Wifi;
import com.romotoapp.constants.Constants;
import com.romotoapp.enums.Permission;
import com.romotoapp.utils.FileUtils;
import com.romotoapp.utils.Utils;

public class SettingsService extends IntentService {
	private static final String TAG = "SettingsService";
	private String userKey;
	private String senderKey;
	private RemoteSettings settings;
	private Wifi wifi;
	private List<AvailableNetworks> availableNetworks;
	private Flight flight;
	private Bluetooth bluetooth;
	private List<PairedDevices> pairedDevices;
	private Sound sound;
	private Volume volume;
	private Display display;
	private Brightness brightness;
	private Storage storage;
	private Battery battery;
	private List<Stats> stats;
	private Apps apps;
	private List<Application> downloaded;
	private List<Service> services;
	private List<Process> processes;
	private List<Application> running;
	private List<Application> allApps;
	private Memory internalStorage;
	private Memory ram;
	private DateTime dateTime;
	private About about;
	private Status status;
	private SIM sim;
	private File[] appIcons;
	
	public SettingsService() {
		super("SettingsService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		userKey = intent.getExtras().getString("user_key");
		senderKey = Utils.getPrefString(SettingsService.this,
				Constants.PROPERTY_USER_KEY);
		initialize();
		loadApplicationData();
		loadBasicSettings();
		loadAboutData();
		loadStatusData();
		loadWifiData();
		loadSettingsData();
		Gson gson = new Gson();
		String json = gson.toJson(settings); 
		File file = FileUtils.saveToFile(this, json.getBytes(), "json");
		Log.d(TAG, file.getAbsolutePath());
		FileUtils.uploadFile(this, new File[]{file}, userKey, senderKey);
		FileUtils.uploadFile(this, appIcons, "", "");
	}
	
	public static void longInfo(String str) {
	    if(str.length() > 4000) {
	        Log.d(TAG, str.substring(0, 4000));
	        longInfo(str.substring(4000));
	    } else
	        Log.d(TAG, str);
	}
	
	
	@SuppressWarnings("deprecation")
	@SuppressLint("NewApi")
	private void loadBasicSettings() {
		boolean flightMode = false;
		boolean autoTime = false;
		boolean autoTimeZone = false;
		boolean bluetoothStatus = false;
		String dateFormat = null;
		int brightness = 0;
		int brightnessMode = 0;
		long screenTimeOut = 0;
		String timeFormat = null;
		int volumeAlarm = 0;
		int volumeMusic = 0;
		int volumeRing = 0;

		WifiManager wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
		WifiInfo wifiInfo = wifiManager.getConnectionInfo();
		TelephonyManager manager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
		ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
		try {
			if (Build.VERSION.SDK_INT < 17) {
				flightMode = Settings.System.getInt(
						getContentResolver(), Settings.System.AIRPLANE_MODE_ON, 0) != 0; 			
				autoTime = Settings.System.getInt(
						getContentResolver(), Settings.System.AUTO_TIME, 0) != 0;
				autoTimeZone = Settings.System.getInt(
						getContentResolver(), Settings.System.AUTO_TIME_ZONE, 0) != 0;
				bluetoothStatus = Settings.System.getInt(
						getContentResolver(), Settings.System.BLUETOOTH_ON, 0) != 0;
			} else {
				flightMode = Settings.Global.getInt(
					getContentResolver(), Settings.Global.AIRPLANE_MODE_ON, 0) != 0;
				autoTime = Settings.Global.getInt(
						getContentResolver(), Settings.Global.AUTO_TIME, 0) != 0;
				autoTimeZone = Settings.Global.getInt(
						getContentResolver(), Settings.Global.AUTO_TIME_ZONE, 0) != 0;
				bluetoothStatus = Settings.Global.getInt(
						getContentResolver(), Settings.Global.BLUETOOTH_ON, 0) != 0;
			}
			dateFormat = Settings.System.getString(
					getContentResolver(), Settings.System.DATE_FORMAT);
			brightness = Settings.System.getInt(
					getContentResolver(), Settings.System.SCREEN_BRIGHTNESS);
			brightnessMode = Settings.System.getInt(
					getContentResolver(), Settings.System.SCREEN_BRIGHTNESS_MODE);
			screenTimeOut = Settings.System.getLong(
					getContentResolver(), Settings.System.SCREEN_OFF_TIMEOUT);
			timeFormat = Settings.System.getString(
					getContentResolver(), Settings.System.TIME_12_24); 
			volumeAlarm = Settings.System.getInt(
					getContentResolver(), Settings.System.VOLUME_ALARM);
			volumeMusic = Settings.System.getInt(
					getContentResolver(), Settings.System.VOLUME_MUSIC);
			volumeRing = Settings.System.getInt(
					getContentResolver(), Settings.System.VOLUME_RING);		
		} catch (SettingNotFoundException e) {
			Log.d(TAG, "Exception occured: " + e.getMessage());
		}
		sim.setNetwork(manager.getNetworkOperatorName());
		sim.setImei(manager.getDeviceId());
		sim.setImeiSv(manager.getDeviceSoftwareVersion());
		if (manager.getLine1Number() == null) {
			sim.setPhoneNumber("unknown");
		}
		if (manager.isNetworkRoaming()) {
			sim.setRoaming("Roaming");
		} else {
			sim.setRoaming("Not roaming");
		}
		if (networkInfo.isConnected()) {
			sim.setMobileNetworkState("Connected");
			sim.setMobileNetworkType(networkInfo.getSubtypeName());
		} else {
			sim.setMobileNetworkState("Disconnected");
			sim.setMobileNetworkType("Not available");
		}
		switch (manager.getCallState()) {
		case ServiceState.STATE_EMERGENCY_ONLY:
			sim.setServiceStatus("Emergency");
			break;
		case ServiceState.STATE_IN_SERVICE:
			sim.setServiceStatus("In Service");
			break;
		case ServiceState.STATE_OUT_OF_SERVICE:
			sim.setServiceStatus("Out of Service");
			break;
		case ServiceState.STATE_POWER_OFF:
			sim.setServiceStatus("Radio Off");
			break;
		default:
			sim.setServiceStatus("Unknown");
		}

		about.setModelNumber(Utils.getDeviceName());
		about.setAndroidVersion(Build.VERSION.RELEASE);
		if (android.os.Build.VERSION.SDK_INT < 14) {
			about.setBasebandVersion(Build.RADIO);
		} else {
			about.setBasebandVersion(Build.getRadioVersion());
		}
		about.setKernelVersion(System.getProperty("os.version"));
		status.setSerialNumber(Build.SERIAL);
		wifi.setStatus(wifiManager.isWifiEnabled());
		status.setWifiMac(wifiInfo.getMacAddress());	
		flight.setStatus(flightMode);
		bluetooth.setStatus(bluetoothStatus);
		dateTime.setAutoDateTime(autoTime);
		dateTime.setAutoTimeZone(autoTimeZone);
		dateTime.setDateFormat(dateFormat);
		dateTime.setTimeFormat(timeFormat);
		this.brightness.setLevel(brightness);
		switch (brightnessMode) {
		case 0:
			this.brightness.setMode("manual");
			break;
		case 1:
			this.brightness.setMode("auto");
		}
		this.volume.setAlarm(volumeAlarm);
		this.volume.setMusic(volumeMusic);
		this.volume.setRingtone(volumeRing);
		display.setSleep(screenTimeOut);
	}
	
	private void loadAboutData() {
	}
	
	private void loadStatusData() {
	}
	
	private void loadWifiData() {
		List<AvailableNetworks> availableNetworks = new ArrayList<AvailableNetworks>();
		WifiManager manager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
		for (WifiConfiguration config : manager.getConfiguredNetworks()) {
			AvailableNetworks network = RemoteSettings.AvailableNetworks.getInstance();
			network.setName(config.SSID);
			if (config.allowedKeyManagement.get(KeyMgmt.WPA_PSK)) {
				network.setSecurity("WPA PSK");
			} else if (config.allowedKeyManagement.get(KeyMgmt.WPA_EAP)) {
				network.setSecurity("WPA EAP");
			} else if (config.allowedProtocols.get(Protocol.WPA)) {
				network.setSecurity("WPA");
			} else {
				network.setSecurity("None");
			}
			availableNetworks.add(network);
		}
		wifi.setAvailableNetworks(availableNetworks);
	}
	
	private void loadSettingsData() {
	}
	
	private void loadApplicationData() {
		int appCount = 0;
		PackageManager packageManager = getPackageManager();
		List<ApplicationInfo> packages = packageManager.getInstalledApplications(PackageManager.GET_META_DATA);
		appIcons = new File[packages.size()];
		int mask = ApplicationInfo.FLAG_SYSTEM | ApplicationInfo.FLAG_UPDATED_SYSTEM_APP;
		for (ApplicationInfo appInfo : packages) {
			final Application application = RemoteSettings.Application.getInstance();
			List<Permissions> permissions = new ArrayList<Permissions>();
			PackageInfo packageInfo;
			try {
				packageInfo = packageManager.getPackageInfo(appInfo.packageName, 0 | PackageManager.GET_PERMISSIONS);
				Bitmap icon = ((BitmapDrawable) appInfo.loadIcon(packageManager)).getBitmap();
				File file = FileUtils.saveToFile(this, FileUtils.convertBitmapToArray(icon), "png");
				appIcons[appCount++] = file;
				application.setIconUrl(file.getName());
				application.setName(packageManager.getApplicationLabel(appInfo).toString());
				application.setVersion(packageInfo.versionName);
				String appPermissions[] = packageInfo.requestedPermissions;
				if (appPermissions != null) {
					for (String appPerm : appPermissions) {
						int lastIndex = appPerm.lastIndexOf(".");
						Permission perm = Permission.valueOf(appPerm.substring(lastIndex + 1));
						if (perm != null) {
							Permissions permission = RemoteSettings.Permissions.getInstance();
							permission.setType(perm.getPermissionType().getType());
							permission.setDescription(perm.getDescription());
							permissions.add(permission);								
							application.setPermissions(permissions);
						}
					}
				}
				Method packageSizeInfo = packageManager.getClass().getMethod(
						"getPackageSizeInfo", String.class, IPackageStatsObserver.class);
				packageSizeInfo.invoke(packageManager, appInfo.packageName, 
						new IPackageStatsObserver.Stub() {
							@Override
							public void onGetStatsCompleted(PackageStats pStats, boolean succeeded)
									throws RemoteException {
								application.setCache(pStats.cacheSize);
								application.setData(pStats.dataSize);
								application.setAppSize(pStats.codeSize);
								application.setTotalSize((pStats.codeSize + pStats.cacheSize + pStats.dataSize));
							}
				});
			} catch (NameNotFoundException e) {
				Log.d(TAG, "Exception occured: " + e.getMessage());
			} catch (NoSuchMethodException e) {
				Log.d(TAG, "Exception occured: " + e.getMessage());
			} catch (IllegalArgumentException e) {
				Log.d(TAG, "Exception occured: " + e.getMessage());
			} catch (IllegalAccessException e) {
				Log.d(TAG, "Exception occured: " + e.getMessage());
			} catch (InvocationTargetException e) {
				Log.d(TAG, "Exception occured: " + e.getMessage());
			}
			if ((appInfo.flags & mask) == 0) {
				downloaded.add(application);
			} else {
				allApps.add(application);
			}
		} 
	}
	
	private void initialize() {
		settings = new RemoteSettings();
		wifi = RemoteSettings.Wifi.getInstance();
		flight = RemoteSettings.Flight.getInstance();
		bluetooth = RemoteSettings.Bluetooth.getInstance();
		sound = RemoteSettings.Sound.getInstance();
		display = RemoteSettings.Display.getInstance();
		storage = RemoteSettings.Storage.getInstance();
		battery = RemoteSettings.Battery.getInstance();
		apps = RemoteSettings.Apps.getInstance();
		dateTime = RemoteSettings.DateTime.getInstance();
		about = RemoteSettings.About.getInstance();

		availableNetworks = new ArrayList<AvailableNetworks>();
		pairedDevices = new ArrayList<PairedDevices>();
		volume = RemoteSettings.Volume.getInstance();
		brightness = RemoteSettings.Brightness.getInstance();
		stats = new ArrayList<Stats>();
		downloaded = new ArrayList<Application>();
		running = new ArrayList<Application>();
		allApps = new ArrayList<Application>();
		internalStorage = RemoteSettings.Memory.getInstance();
		ram = RemoteSettings.Memory.getInstance();
		status = RemoteSettings.Status.getInstance();
		
		services = new ArrayList<Service>();
		processes = new ArrayList<Process>();
		sim = RemoteSettings.SIM.getInstance();
		
		availableNetworks.add(RemoteSettings.AvailableNetworks.getInstance());
		pairedDevices.add(RemoteSettings.PairedDevices.getInstance());
		stats.add(RemoteSettings.Stats.getInstance());
		services.add(RemoteSettings.Service.getInstance());
		processes.add(RemoteSettings.Process.getInstance());
		
		wifi.setAvailableNetworks(availableNetworks);
		bluetooth.setPairedDevices(pairedDevices);
		sound.setVolume(volume);
		display.setBrightness(brightness);
		battery.setStats(stats);
		
		apps.setDownloaded(downloaded);
		apps.setRunning(running);
		apps.setAll(allApps);
		apps.setInternalStorage(internalStorage);
		apps.setRam(ram);
		status.setSim(sim);
		status.setBattery(battery);
		about.setStatus(status);
		
		settings.setWifi(wifi);
		settings.setFlight(flight);
		settings.setBluetooth(bluetooth);
		settings.setSound(sound);
		settings.setDisplay(display);
		settings.setStorage(storage);
		settings.setBattery(battery);
		settings.setApps(apps);
		settings.setDateTime(dateTime);
		settings.setAbout(about);
	}
}
