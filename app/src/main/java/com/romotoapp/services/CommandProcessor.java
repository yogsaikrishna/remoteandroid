package com.romotoapp.services;

import java.util.List;

import android.app.IntentService;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;

import com.romotoapp.RemoteAction;
import com.romotoapp.constants.SessionConstants;
import com.romotoapp.enums.Command;
import com.romotoapp.interfaces.OnFileDownloadListener;
import com.romotoapp.utils.CommandActions;
import com.romotoapp.utils.FileUtils;

public class CommandProcessor extends IntentService implements OnFileDownloadListener {
	private static final String TAG = "CommandProcessor";
	private Bundle bundle;
	private String cmd;
	private String fileName;
	
	public CommandProcessor() {
		super("CommandProcessor");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		bundle = intent.getExtras();
		fileName = bundle.getString("url");
		cmd = bundle.getString("command");
		Command command = Command.fromString(cmd);
		doAction(command);
	}
	
	private void doAction(Command command) {
		switch (command) {
		case URL:
			Log.d(TAG, "" + bundle.getString("url"));
			FileUtils.downloadFile(this, fileName);
			break;
		case ALERT:
			break;
		case AIRPLANE_OFF:
			Log.d(TAG, command.getCommand());
			break;
		case AIRPLANE_ON:
			Log.d(TAG, command.getCommand());
			break;
		case BLUETOOTH_OFF:
			Log.d(TAG, command.getCommand());
			break;
		case BLUETOOTH_ON:
			Log.d(TAG, command.getCommand());
			break;
		case CONNECT:
			SessionConstants.clear();
			SessionConstants.isSessionStarted = true;				
			Log.d(TAG, command.getCommand());
			CommandActions.alert(getApplicationContext(), bundle);
			break;
		case ACCEPT:
			SessionConstants.isRemoteAccept = true;
			CommandActions.alert(getApplicationContext(), bundle);
			break;
		case REJECT:
			SessionConstants.isRemoteReject = true;
			CommandActions.alert(getApplicationContext(), bundle);
			break;
		case WIFI_OFF:
			Log.d(TAG, command.getCommand());
			break;
		case WIFI_ON:
			Log.d(TAG, command.getCommand());
			break;
		default:
			Log.d(TAG, command.getCommand());
			break;
		}		
	}

	@Override
	public void onFileDownload(String jsonString) {
		bundle.putString("json", jsonString);
		Intent intent = new Intent(CommandProcessor.this, RemoteAction.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.putExtras(bundle);
		startActivity(intent);
	}

	@Override
	public void onFileDownload(List<Bitmap> bitmaps) {
		
	}

}
