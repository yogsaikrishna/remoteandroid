package com.romotoapp;

import java.util.List;
import java.util.Locale;
import java.util.Vector;

import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.SearchView.OnQueryTextListener;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.romotoapp.common.ListAdapter;
import com.romotoapp.common.RemoteUser;
import com.romotoapp.common.RowData;
import com.romotoapp.constants.Constants;
import com.romotoapp.constants.SessionConstants;
import com.romotoapp.helpers.AlertDialogHelper;
import com.romotoapp.helpers.DBHelper;
import com.romotoapp.interfaces.OnDataSyncListener;
import com.romotoapp.utils.DataSyncTask;
import com.romotoapp.utils.Utils;

public class RemoteDevices extends BaseActivity implements OnQueryTextListener,
		OnItemClickListener, OnItemLongClickListener, MenuItemCompat.OnActionExpandListener,
		OnDataSyncListener {
	private static final String TAG = "RemoteDevices";

	private ListView listView;
	private TextView tvListDesc;
	private Vector<RowData> data;
	private RowData rowData;
	private ListAdapter listAdapter;
	private List<RemoteUser> devices;
	private DBHelper db;
	private ActionMode actionMode;
	private boolean selectionMode;
	private boolean listChanged;
	private DataSyncTask dataSyncTask;
	private ProgressDialog progressDialog;
	private SparseBooleanArray checked;
	private Thread thread;
	private String userKey;
	private AlertDialog.Builder alertDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.list_layout);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		db = new DBHelper(this);
		devices = db.getAllUsers();
		userKey = Utils.getPrefString(RemoteDevices.this, Constants.PROPERTY_USER_KEY);
		
		listView = (ListView) findViewById(R.id.listView);
		tvListDesc = (TextView) findViewById(R.id.tv_list_desc);
		checked = new SparseBooleanArray();
		
		displayDeviceList();
		listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
		listView.setOnItemClickListener(this);
		listView.setOnItemLongClickListener(this);
	}

	private String splitUserKey(String userKey) {
		char[] array = userKey.toCharArray();
		StringBuffer key = new StringBuffer();
		for (int i = 0; i < array.length; i++) {
			if (i == 2 || i == 5) {
				key.append(array[i]);
				key.append("-");
			} else {
				key.append(array[i]);
			}
		}
		return key.toString();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			this.finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.remote_devices_actions, menu);
		MenuItem searchMenuItem = menu.findItem(R.id.device_search);
		SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
		SearchView searchView = (SearchView) MenuItemCompat
				.getActionView(searchMenuItem);
		View searchPlate = (View) searchView
				.findViewById(android.support.v7.appcompat.R.id.search_plate);
		searchPlate
				.setBackgroundResource(R.drawable.rm_action_edit_text_holo_light);
		searchView.setSearchableInfo(searchManager
				.getSearchableInfo(getComponentName()));
		searchView.setOnQueryTextListener(this);
		MenuItemCompat.setOnActionExpandListener(searchMenuItem, this);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onQueryTextChange(String text) {
		if (TextUtils.isEmpty(text)) {
			displayDeviceList();
			listAdapter.notifyDataSetChanged();
		} else {
			data = new Vector<RowData>();
			for (RemoteUser user : devices) {
				if (user.getSenderName().toLowerCase(Locale.getDefault())
						.startsWith(text.toLowerCase(Locale.getDefault()))) {
					rowData = new RowData(user.getSenderName(), user.getModel()
							+ " [ " + splitUserKey(user.getSenderId()) + " ]", R.drawable.ic_action_phone ,true);
					data.add(rowData);
				}
			}
			listAdapter = new ListAdapter(this, R.layout.device_list_item,
					R.id.tv_text, data);
			listView.setAdapter(listAdapter);
			listAdapter.notifyDataSetChanged();
		}
		return true;
	}

	@Override
	public boolean onQueryTextSubmit(String text) {
		return false;
	}

	private void displayDeviceList() {
		if (devices.size() > 0) {
			tvListDesc.setVisibility(View.GONE);
		} else {
			tvListDesc.setVisibility(View.VISIBLE);
		}
		int count = 0;
		data = new Vector<RowData>();
		for (RemoteUser remoteUser : devices) {
			rowData = new RowData(remoteUser.getSenderName(),
					remoteUser.getModel() + " [ "
							+ splitUserKey(remoteUser.getSenderId()) + " ]", R.drawable.ic_action_phone, true);
			data.add(rowData);
			checked.put(count, false);
			count += 1;
		}
		listAdapter = new ListAdapter(this, R.layout.device_list_item,
				R.id.tv_text, data);
		listView.setAdapter(listAdapter);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
		if (selectionMode) {
			checked.put(position, !checked.valueAt(position));
			boolean hasCheckedElement = false;
			for (int i = 0; i < checked.size() & !hasCheckedElement; i++) {
				hasCheckedElement = checked.valueAt(i);
			}
			if (hasCheckedElement) {
				if (actionMode == null) {
					actionMode = startSupportActionMode(new ActionModeCallback());
				}
				actionMode.setTitle(getSelectedCount() +  " selected");
				toggleListSelection();
			} else {
				if (actionMode != null) {
					actionMode.finish();
					selectionMode = false;
				}
			}			
		} else {
			alertDialog = new AlertDialog.Builder(RemoteDevices.this);
			alertDialog.setMessage("Do you want to connect to " 
					+ devices.get(position).getSenderName() + " device?")
				.setNegativeButton("No", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				})
				.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						progressDialog = ProgressDialog.show(RemoteDevices.this, "", "Connecting...");
						SessionConstants.clear();
						dataSyncTask = new DataSyncTask(RemoteDevices.this);
						dataSyncTask.execute(Constants.URL_COMMAND
								+ "sender_key=" + userKey
								+ "&user_key=" + devices.get(position).getSenderId()
								+ "&command=" + "connect");						
						SessionConstants.isSessionStarted = true;
						dialog.dismiss();
					}
				});
			alertDialog.show();
		}
	}
	
	private final class ActionModeCallback implements ActionMode.Callback {

		@Override
		public boolean onActionItemClicked(ActionMode mode, MenuItem menuItem) {
			switch (menuItem.getItemId()) {
			case R.id.device_delete:
				SparseBooleanArray selected = listView.getCheckedItemPositions();
				if (selected.size() > 0) {
					for (int i = 0; i < selected.size(); i++) {
						if (selected.valueAt(i)) {
							long id = devices.get(selected.keyAt(i)).getId();
							db.deleteRemoteUser(id);							
						}
					}
					listChanged = true;
					devices = db.getAllUsers();
					toggleListSelection();
					displayDeviceList();
					listAdapter.notifyDataSetChanged();
				}
				mode.finish();
				break;
			}
			return true;
		}

		@Override
		public boolean onCreateActionMode(ActionMode mode, Menu menu) {
			MenuInflater inflater = getMenuInflater();
			inflater.inflate(R.menu.remote_devices_actions, menu);
			return true;
		}

		@Override
		public void onDestroyActionMode(ActionMode mode) {
			for (int i = 0; i < checked.size(); i++) {
				checked.put(i, false);
			}
			if (mode == actionMode) {
				actionMode = null;
				selectionMode = false;
			}
			if (!listChanged) {
				toggleListSelection();
			}
		}

		@Override
		public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
			return false;
		}
		
	}

	@Override
	public boolean onItemLongClick(AdapterView<?> parent, View view, int position,
			long id) {
		if (!selectionMode) {
			selectionMode = true;
			actionMode = startSupportActionMode(new ActionModeCallback());			
		}
		return false;
	}
	
	private void toggleListSelection() {
		View view = null;
		for (int i = 0; i < checked.size(); i++) {
			view = listView.getChildAt(checked.keyAt(i));
			if (checked.valueAt(i)) {
				view.setBackgroundResource(R.drawable.rm_list_pressed_holo_light);
			} else {
				view.setBackgroundResource(R.drawable.rm_list_selector_holo_light);				
			}			
		}
	}
	
	public int getSelectedCount() {
		int selectedCount = 0;
		for (int i = 0; i < checked.size(); i++) {
			if (checked.valueAt(i)) {
				selectedCount += 1;
			}			
		}
		return selectedCount;
	}
	
	@Override
	protected void onPause() {
		db.closeDB();
		super.onPause();
	}

	@Override
	public boolean onMenuItemActionCollapse(MenuItem menuItem) {
		displayDeviceList();
		return true;
	}

	@Override
	public boolean onMenuItemActionExpand(MenuItem menuItem) {
		return true;
	}

	@Override
	public void onSyncComplete(JSONObject jsonObj) {
		progressDialog.dismiss();
		progressDialog = ProgressDialog.show(RemoteDevices.this, "", "Waiting for approval...");
		thread = new Thread(new Runnable() {
	
			@Override
			public void run() {
				try {
					for (int count = 0; count < 10; count ++) {
						Thread.sleep(1000);
						if (SessionConstants.isRemoteAccept
								|| SessionConstants.isRemoteReject) {
							progressDialog.dismiss();
							thread.interrupt();
						}
					}
					progressDialog.dismiss();
					if (!SessionConstants.isRemoteReject
							&& !SessionConstants.isRemoteAccept) {
						Intent intent = new Intent(RemoteDevices.this, AlertDialogHelper.class);
						intent.putExtra("sender_key", userKey);
						intent.putExtra("command", "alert");
						startActivity(intent);			
					}
				} catch (InterruptedException e) {
					Log.e(TAG, "Exception occured: " + e.getMessage());
				}
			}
		});
		thread.start();
	}

}
