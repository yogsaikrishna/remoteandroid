package com.romotoapp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.romotoapp.constants.Constants;
import com.romotoapp.utils.GCMUtils;
import com.romotoapp.utils.Utils;

public class RemoteAndroid extends BaseActivity {
	private static final String TAG = "RemoteAndroid";
	
	private Context context;
	private String regId;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		context = getApplicationContext();
		if (GCMUtils.checkPlayServices(this)) {
			regId = GCMUtils.getRegistrationId(context);
			if (regId == null) {
				GCMUtils.register(context);
			}
			if (!Utils.getPrefBoolean(RemoteAndroid.this, Constants.PROPERTY_FIRST_RUN)) {
				Intent intent = new Intent(RemoteAndroid.this, Registration.class);
				startActivity(intent);
			} else {
				Intent intent = new Intent(RemoteAndroid.this, RemoteHome.class);
				startActivity(intent);				
			}
			finish();
		} else {
			Log.i(TAG, "No valid Google Play Services APK found");
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		GCMUtils.checkPlayServices(this);
	}

}
