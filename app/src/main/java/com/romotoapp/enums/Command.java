package com.romotoapp.enums;

public enum Command {
	
	URL("url"),
	ALERT("alert"),
	CONNECT("connect"),
	ACCEPT("accept"),
	REJECT("reject"),
	WIFI_ON("wifi_on"),
	WIFI_OFF("wifi_off"),
	AIRPLANE_ON("airplane_on"),
	AIRPLANE_OFF("airplane_off"),
	BLUETOOTH_ON("bluetooth_on"),
	BLUETOOTH_OFF("bluetooth_off");
	
	private String command;
	
	private Command(String command) {
		this.command = command;
	}
	
	public String getCommand() {
		return command;
	}
	
	public static Command fromString(String cmd) {
		if (cmd != null) {
			for (Command c : Command.values()) {
				if (cmd.equalsIgnoreCase(c.command)) {
					return c;
				}
			}
		}
		return null;
	}
}
