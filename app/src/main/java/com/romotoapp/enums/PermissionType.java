package com.romotoapp.enums;

public enum PermissionType {
	ACCOUNTS("Your Accounts"),
	AFFECTS_BATTERY("Affects Battery"),
	ALARM("Alarm"),
	APPLICATION_INFO("Your Application Information"),
	AUDIO("Audio Settings"),
	BLUETOOTH("Bluetooth"),
	CAMERA("Camera"),
	CLOCK("Clock"),
	DICTIONARY("Read User Dictionary"),
	HARDWARE("Hardware Controls"),
	HISTORY_BOOKMARKS("Bookmarks and History"),
	LOCATION("Your Location"),
	LOCK_SCREEN("Lock Screen"),
	MESSAGING("Your Messages"),
	MICROPHONE("Microphone"),
	NETWORK_COMMUNICATION("Network Communication"),
	NONE(""),
	OTHER("Others"),
	PERSONAL_INFO("Your Personal Information"),
	PHONE_CALLS("Phone Calls"),
	STATUS_BAR("Status Bar"),
	STORAGE("Storage"),
	SOCIAL_INFO("Your Social Information"),
	SYNC("Sync Settings"),
	SYSTEM_TOOLS("System Tools"),
	VOICEMAIL("Voicemail"),
	WALLPAPER("Wallpaper");
	
	private String type;
	
	private PermissionType(String type) {
		this.type = type;
	}
	
	public String getType() {
		return type;
	}
}
