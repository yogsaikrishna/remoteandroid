package com.romotoapp;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerTabStrip;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;

import com.romotoapp.actions.AdvancedActionsFragment;
import com.romotoapp.actions.BasicActionsFragment;
import com.romotoapp.actions.IntermediateActionsFragment;
import com.romotoapp.constants.SessionConstants;

public class RemoteAction extends BaseActivity {
	private static String[] titles = {"BASIC", "INTERMEDIATE", "ADVANCED"};
	private PagerAdapter pagerAdapter;
	private PagerTabStrip pagerTabStrip;
	private ViewPager viewPager;
	private AlertDialog.Builder alertDialog;
	private Bundle bundle;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		bundle = getIntent().getExtras();
		setContentView(R.layout.tab_strip_layout);
		pagerAdapter = new PagerAdapter(getSupportFragmentManager(), bundle);
		final ActionBar actionBar = getSupportActionBar();
		actionBar.setHomeButtonEnabled(false);
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		pagerTabStrip = (PagerTabStrip) findViewById(R.id.pager_title_strip);
		viewPager = (ViewPager) findViewById(R.id.pager);
		viewPager.setAdapter(pagerAdapter);
		pagerTabStrip.setTabIndicatorColorResource(R.color.color);
	}
	
	public static class PagerAdapter extends FragmentPagerAdapter {
		private Bundle bundle;
		
		public PagerAdapter(FragmentManager fm, Bundle bundle) {
			super(fm);
			this.bundle = bundle;
		}

		@Override
		public Fragment getItem(int i) {
			Fragment fragment;
			switch (i) {
			case 0:
				fragment = new BasicActionsFragment();
				fragment.setArguments(bundle);
				return fragment;
			case 1:
				fragment = new IntermediateActionsFragment();
				fragment.setArguments(bundle);
				return fragment;
			case 2:
				fragment = new AdvancedActionsFragment();
				fragment.setArguments(bundle);
				return fragment;
			default:
				fragment = new BasicActionsFragment();
				fragment.setArguments(bundle);
				return fragment;
			}
		}

		@Override
		public int getCount() {
			return 3;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			return titles[position];
		}
		
	}

	@Override
	public void onBackPressed() {
		alertDialog = new AlertDialog.Builder(this);
		alertDialog.setMessage("Do you want to end the session?")
			.setNegativeButton("No", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
				}
			})
			.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
					SessionConstants.clear();
					finish();
				}
			});
		alertDialog.show();
	}
}
