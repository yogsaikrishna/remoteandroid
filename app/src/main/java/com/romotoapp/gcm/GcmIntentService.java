package com.romotoapp.gcm;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.romotoapp.services.CommandProcessor;

public class GcmIntentService extends IntentService {
	private static final String TAG = "GcmIntentService";

	public GcmIntentService() {
		super("GcmIntentService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		Bundle bundle = intent.getExtras();
		GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
		String messageType = gcm.getMessageType(intent);
		
		if (!bundle.isEmpty()) {
			if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
				//Utils.sendNotification(GcmIntentService.this, "Send error: " + extras.toString());
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) {
            	//Utils.sendNotification(GcmIntentService.this, "Deleted messages on server: " + extras.toString());
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
            	//Utils.sendNotification(GcmIntentService.this, bundle);
            	Intent i = new Intent(GcmIntentService.this, CommandProcessor.class);
            	i.putExtras(bundle);
            	startService(i);
			}
		}
		Log.d(TAG, bundle.getString("sender"));
		Log.d(TAG, bundle.getString("sender_id"));
		Log.d(TAG, bundle.getString("sender_key"));
		Log.d(TAG, bundle.getString("model"));
		Log.d(TAG, bundle.getString("command"));
		GcmBroadcastReceiver.completeWakefulIntent(intent);
	}

}
