package com.romotoapp;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

public class BaseActivity extends ActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		int apiVersion = android.os.Build.VERSION.SDK_INT;
		if (apiVersion < 7) {
			setTheme(android.R.style.Theme_Light);
		}
		super.onCreate(savedInstanceState);
	}

}