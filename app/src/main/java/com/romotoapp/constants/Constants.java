package com.romotoapp.constants;

public class Constants {
	public static final String URL_REGISTRATION = "http://romotoapp.com/a14d18o9d/r15m15t15/register.php?";
	public static final String URL_COMMAND = "http://romotoapp.com/a14d18o9d/r15m15t15/sendcommand.php?";
	public static final String URL_PUSH_COMMAND = "http://romotoapp.com/a14d18o9d/r15m15t15/pushcommand.php?";
	public static final String URL_UPLOAD = "http://romotoapp.com/a14d18o9d/r15m15t15/upload.php?";
	public static final String URL_DOWNLOAD = "http://romotoapp.com/a14d18o9d/r15m15t15/u16l15a419/d1t1/";
	public static final String URL_FILE_EXISTS = "http://www.romotoapp.com/a14d18o9d/r15m15t15/fileexists?file_name=";
	
	public static final String WARN_NAME = "Please enter your name";
	public static final String WARN_EMAIL = "Please enter your email";
	public static final String WARN_MOBILE = "Please enter your mobile no.";
	public static final String WARN_INVALID_MOBILE = "Please enter valid mobile no.";
	public static final String WARN_NO_NETWORK = "Network connection not available";
	
	public static final String ERR_TRY_AGAIN = "Error please try again";
	
	public static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
	public static final String SENDER_ID = "815444104387";
	
	public static final String PREFERENCE_NAME = "remoteandroid";
	public static final String PROPERTY_REG_ID = "registrationId";
	public static final String PROPERTY_APP_VERSION = "appVersion";
	public static final String PROPERTY_ID = "id";
	public static final String PROPERTY_USER_KEY = "user_key";
	public static final String PROPERTY_NAME = "name";
	public static final String PROPERTY_EMAIL = "email";
	public static final String PROPERTY_MOBILE_NO = "mobile_no";
	public static final String PROPERTY_MODEL = "model";
	public static final String PROPERTY_STATUS = "status";
	public static final String PROPERTY_FIRST_RUN = "firstRun";
	
	public static final int NOTIFICATION_ID = 2890;
	public static final int MB = 1048576;
	public static final int KB = 1024;
	
	public static final int DENSITY_LOW = 120;
	public static final int DENSITY_MEDIUM = 160;
	public static final int DENSITY_HIGH = 240;
	public static final int DENSITY_XHIGH = 320;
	public static final int DENSITY_XXHIGH = 480;
	
	public static final int DENSITY_UNSCALED = 1;
	public static final int DENSITY_SCALED = 2;
	
}
