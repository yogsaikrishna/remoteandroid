package com.romotoapp.constants;

import java.util.List;

import android.graphics.Bitmap;

public class SessionConstants {
	public static boolean isSessionStarted = false;
	public static boolean isRemoteAccept = false;
	public static boolean isRemoteReject = false;
	public static boolean isCommandReceived = false;
	public static List<Bitmap> appIcons = null;
	public static List<Bitmap> allAppIcons = null;
	
	public static void clear() {
		isSessionStarted = false;
		isRemoteAccept = false;
		isRemoteReject = false;
		appIcons = null;
		allAppIcons = null;
	}
}
