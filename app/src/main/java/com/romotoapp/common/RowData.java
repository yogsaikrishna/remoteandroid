package com.romotoapp.common;

import android.graphics.Bitmap;

public class RowData {
	private String text;
	private String secondaryText;
	private int resource;
	private Bitmap bmpImage;
	private String count;
	private boolean imgVisible;
	
	public RowData(String text, int resource) {
		this.text = text;
		this.resource = resource;
	}
	
	public RowData(String text, String secondaryText) {
		this.text = text;
		this.secondaryText = secondaryText;
	}
	
	public RowData(String text, String secondaryText, int resource, boolean imgVisible) {
		this.text = text;
		this.secondaryText = secondaryText;
		this.resource = resource;
		this.imgVisible = imgVisible;
	}
	
	public RowData(String text, String secondaryText, Bitmap bmpImage, boolean imgVisible) {
		this.text = text;
		this.secondaryText = secondaryText;
		this.bmpImage = bmpImage;
		this.imgVisible = imgVisible;
	}
	
	public RowData(String text, int resource, String count) {
		this.text = text;
		this.resource = resource;
		this.count = count;
	}
	
	public String getText() {
		return text;
	}
	
	public void setText(String text) {
		this.text = text;
	}
	
	public int getResource() {
		return resource;
	}
	
	public void setResource(int resource) {
		this.resource = resource;
	}

	public String getSecondaryText() {
		return secondaryText;
	}

	public void setSecondaryText(String secondaryText) {
		this.secondaryText = secondaryText;
	}

	public String getCount() {
		return count;
	}

	public void setCount(String count) {
		this.count = count;
	}

	public boolean isImgVisible() {
		return imgVisible;
	}

	public void setImgVisible(boolean imgVisible) {
		this.imgVisible = imgVisible;
	}

	public Bitmap getBmpImage() {
		return bmpImage;
	}

	public void setBmpImage(Bitmap bmpImage) {
		this.bmpImage = bmpImage;
	}
}
