package com.romotoapp.common;

import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.romotoapp.R;
import com.romotoapp.constants.Constants;
import com.romotoapp.utils.Utils;

public class ListAdapter extends ArrayAdapter<RowData> {
	private Context context;
	private int resource;
	
	public ListAdapter(Context context, int resource,
			int textViewResourceId, List<RowData> objects) {
		super(context, resource, textViewResourceId, objects);
		this.context = context;
		this.resource = resource;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
    	TextView title = null;
        ImageView image = null;
        TextView secondaryText = null;
        TextView count = null;
        TextView user = null;
        LinearLayout userLayout = null;
        FrameLayout rowLayout = null;
        
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        RowData row =  (RowData) getItem(position);

        if (convertView == null) {
        	convertView = inflater.inflate(resource, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);                   
        }
        
        holder = (ViewHolder) convertView.getTag();
        
        title = holder.getText();
        title.setText(row.getText());
    	image = holder.getImage();

        switch (resource) {
        case R.layout.drawer_list_item:
        	count = holder.getCount();
        	user = holder.getUser();
        	String userName = Utils.getPrefString(context, Constants.PROPERTY_NAME);
        	String email = Utils.getPrefString(context, Constants.PROPERTY_EMAIL);
        	user.setText(Html.fromHtml("<b>" + userName + "</b><br/><small><i>" + email + "</i></small>"));
        	userLayout = holder.getUserLayout();
        	rowLayout = holder.getRowLayout();
        	if (row.getText() != null
        		&& row.getResource() != -1) {
        		userLayout.setVisibility(View.GONE);
                image.setImageResource(row.getResource());        		
                if (row.getCount() != null) {
                	count.setText(row.getCount());
                } else {
                	count.setVisibility(View.GONE);
                }
        	} else {
        		rowLayout.setVisibility(View.GONE);
        	}
        	break;
        case R.layout.device_list_item:
        	LinearLayout.LayoutParams layoutParams = null;
        	int size = Utils.getDisplaySize(context, Constants.DENSITY_UNSCALED);
			layoutParams = new LinearLayout.LayoutParams(size, size);
			image.setLayoutParams(layoutParams);
        	if (row.isImgVisible()) {
        		Bitmap bmp = row.getBmpImage();
        		if (bmp != null) {
        			image.setImageBitmap(bmp);
        		} else {
        			image.setImageResource(row.getResource());	
        		}
        	} else {
        		image.setVisibility(View.GONE);
        	}
        	secondaryText = holder.getSecondaryText();
        	secondaryText.setText(row.getSecondaryText());
        	break;
        }
        
		return convertView;
	}

	@Override
	public int getItemViewType(int position) {
		// TODO Auto-generated method stub
		return super.getItemViewType(position);
	}

	@Override
	public int getViewTypeCount() {
		// TODO Auto-generated method stub
		return super.getViewTypeCount();
	}
}
