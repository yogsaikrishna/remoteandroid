package com.romotoapp.common;

public class RemoteUser {
	private long id;
	private String senderId;
	private String senderName;
	private String senderGcmId;
	private String model;
	private String created;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getSenderId() {
		return senderId;
	}
	public void setSenderId(String senderId) {
		this.senderId = senderId;
	}
	public String getSenderName() {
		return senderName;
	}
	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}
	public String getSenderGcmId() {
		return senderGcmId;
	}
	public void setSenderGcmId(String senderGcmId) {
		this.senderGcmId = senderGcmId;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getCreated() {
		return created;
	}
	public void setCreated(String created) {
		this.created = created;
	}
	
}
