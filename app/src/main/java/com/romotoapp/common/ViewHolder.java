package com.romotoapp.common;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.romotoapp.R;

public class ViewHolder {
	private View view;
	private TextView  text = null;
	private ImageView image = null;
	private TextView secondaryText = null;
	private TextView count = null;
	private TextView user = null;
	private LinearLayout userLayout = null;
	private FrameLayout rowLayout = null;
	
	
	public ViewHolder(View view) {
		this.view = view;
	}

	public TextView getText() {
		if (text == null) {
			text = (TextView) view.findViewById(R.id.tv_text);
		}
		return text;
	}

	public ImageView getImage() {
		if (image == null) {
			image = (ImageView) view.findViewById(R.id.iv_image);
		}
		return image;
	}
	
	public TextView getSecondaryText() {
		if (secondaryText == null) {
			secondaryText = (TextView) view.findViewById(R.id.tv_secondary_text);
		}
		return secondaryText;
	}
	
	public TextView getCount() {
		if (count == null) {
			count = (TextView) view.findViewById(R.id.tv_count);
		}
		return count;
	}
	
	public TextView getUser() {
		if (user == null) {
			user = (TextView) view.findViewById(R.id.tv_user);
		}
		return user;
	}
	
	public LinearLayout getUserLayout() {
		if (userLayout == null) {
			userLayout = (LinearLayout) view.findViewById(R.id.user_layout);
		}
		return userLayout;
	}
	
	public FrameLayout getRowLayout() {
		if (rowLayout == null) {
			rowLayout = (FrameLayout) view.findViewById(R.id.row_layout);
		}
		return rowLayout;
	}
}
