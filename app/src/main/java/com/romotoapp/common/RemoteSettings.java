package com.romotoapp.common;

import java.util.ArrayList;
import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;


public class RemoteSettings implements Parcelable {
	private Wifi wifi;
	private Flight flight;
	private Bluetooth bluetooth;
	private Sound sound;
	private Display display;
	private Storage storage;
	private Battery battery;
	private Apps apps;
	private DateTime dateTime;
	private About about;
	
	public RemoteSettings() {
		
	}
	
	private RemoteSettings(Parcel in) {
		wifi = (Wifi) in.readParcelable(Wifi.class.getClassLoader());
		flight = (Flight) in.readParcelable(Flight.class.getClassLoader());
		bluetooth = (Bluetooth) in.readParcelable(Flight.class.getClassLoader());
		sound = (Sound) in.readParcelable(Flight.class.getClassLoader());
		display = (Display) in.readParcelable(Flight.class.getClassLoader());
		storage = (Storage) in.readParcelable(Flight.class.getClassLoader());
		battery = (Battery) in.readParcelable(Flight.class.getClassLoader());
		apps = (Apps) in.readParcelable(Flight.class.getClassLoader());
		dateTime = (DateTime) in.readParcelable(Flight.class.getClassLoader());
		about = (About) in.readParcelable(Flight.class.getClassLoader());
	}
	
	public Wifi getWifi() {
		return wifi;
	}
	public Flight getFlight() {
		return flight;
	}
	public Bluetooth getBluetooth() {
		return bluetooth;
	}
	public Sound getSound() {
		return sound;
	}
	public Display getDisplay() {
		return display;
	}
	public Storage getStorage() {
		return storage;
	}
	public Battery getBattery() {
		return battery;
	}
	public Apps getApps() {
		return apps;
	}
	public DateTime getDateTime() {
		return dateTime;
	}
	public About getAbout() {
		return about;
	}
	public void setWifi(Wifi wifi) {
		this.wifi = wifi;
	}
	public void setFlight(Flight flight) {
		this.flight = flight;
	}
	public void setBluetooth(Bluetooth bluetooth) {
		this.bluetooth = bluetooth;
	}
	public void setSound(Sound sound) {
		this.sound = sound;
	}
	public void setDisplay(Display display) {
		this.display = display;
	}
	public void setStorage(Storage storage) {
		this.storage = storage;
	}
	public void setBattery(Battery battery) {
		this.battery = battery;
	}
	public void setApps(Apps apps) {
		this.apps = apps;
	}
	public void setDateTime(DateTime dateTime) {
		this.dateTime = dateTime;
	}
	public void setAbout(About about) {
		this.about = about;
	}        

	public static class Wifi implements Parcelable {
		private boolean status;
		private List<AvailableNetworks> availableNetworks;
		
		public Wifi() {
			availableNetworks = new ArrayList<AvailableNetworks>();
		}
		
		public Wifi(Parcel in) {
			this();
			status = in.readByte() != 0;
			in.readTypedList(availableNetworks, AvailableNetworks.CREATOR);
		}
		
		public boolean isStatus() {
			return status;
		}
		public List<AvailableNetworks> getAvailableNetworks() {
			return availableNetworks;
		}
		public void setStatus(boolean status) {
			this.status = status;
		}
		public void setAvailableNetworks(
				List<AvailableNetworks> availableNetworks) {
			this.availableNetworks = availableNetworks;
		}
		
		public static Wifi getInstance() {
			return new Wifi();
		}
		
		@Override
		public int describeContents() {
			return 0;
		}
		@Override
		public void writeToParcel(Parcel dest, int flags) {
			dest.writeByte((byte) (status ? 1 : 0));
			dest.writeTypedList(availableNetworks);
		}
		
		public static final Parcelable.Creator<Wifi> CREATOR = new Parcelable.Creator<Wifi>() {

			@Override
			public Wifi createFromParcel(Parcel source) {
				return new Wifi(source);
			}

			@Override
			public Wifi[] newArray(int size) {
				return new Wifi[size];
			}
		};
	}

	public static class Flight implements Parcelable {
		private boolean status;

		public Flight() {
			
		}
		
		public Flight(Parcel in) {
			status = in.readByte() != 0;
		}

		public boolean isStatus() {
			return status;
		}

		public void setStatus(boolean status) {
			this.status = status;
		}
		
		public static Flight getInstance() {
			return new Flight();
		}

		@Override
		public int describeContents() {
			return 0;
		}

		@Override
		public void writeToParcel(Parcel dest, int flags) {
			dest.writeByte((byte) (status ? 1 : 0));
		}
		
		public static final Parcelable.Creator<Flight> CREATOR = new Parcelable.Creator<Flight>() {

			@Override
			public Flight createFromParcel(Parcel source) {
				return new Flight(source);
			}

			@Override
			public Flight[] newArray(int size) {
				return new Flight[size];
			}
		};
	}
	
	public static class Bluetooth implements Parcelable {
		private boolean status;
		private String name;
		private List<PairedDevices> pairedDevices;
		private double timeOut;

		public Bluetooth() {
			pairedDevices = new ArrayList<PairedDevices>();
		}
		
		public Bluetooth(Parcel in) {
			this();
			status = in.readByte() != 0;
			name = in.readString();
			in.readTypedList(pairedDevices, PairedDevices.CREATOR);
			timeOut = in.readDouble();
		}
		
		public boolean isStatus() {
			return status;
		}
		public String getName() {
			return name;
		}
		public List<PairedDevices> getPairedDevices() {
			return pairedDevices;
		}
		public double getTimeOut() {
			return timeOut;
		}
		public void setStatus(boolean status) {
			this.status = status;
		}
		public void setName(String name) {
			this.name = name;
		}
		public void setPairedDevices(List<PairedDevices> pairedDevices) {
			this.pairedDevices = pairedDevices;
		}
		public void setTimeOut(double timeOut) {
			this.timeOut = timeOut;
		}
		
		public static Bluetooth getInstance() {
			return new Bluetooth();
		}
		
		@Override
		public int describeContents() {
			return 0;
		}
		@Override
		public void writeToParcel(Parcel dest, int flags) {
			dest.writeByte((byte) (status ? 1 : 0));
			dest.writeString(name);
			dest.writeTypedList(pairedDevices);
			dest.writeDouble(timeOut);
		}
		
		public static final Parcelable.Creator<Bluetooth> CREATOR = new Parcelable.Creator<Bluetooth>() {

			@Override
			public Bluetooth createFromParcel(Parcel source) {
				return new Bluetooth(source);
			}

			@Override
			public Bluetooth[] newArray(int size) {
				return new Bluetooth[size];
			}
		};
	}
	
	public static class Sound implements Parcelable {
		private Volume volume;
		private String ringtone;
		private boolean vibrateWhenRing;
		private String notificationSound;
		private boolean dialPadTones;
		private boolean touchSounds;
		private boolean screenLockSounds;
		private boolean vibrateOnTouch;
		
		public Sound() {
			
		}
		
		public Sound(Parcel in) {
			volume = (Volume) in.readParcelable(Volume.class.getClassLoader());
			ringtone = in.readString();
			vibrateWhenRing = in.readByte() != 0;
			notificationSound = in.readString();
			dialPadTones = in.readByte() != 0;
			touchSounds = in.readByte() != 0;
			screenLockSounds = in.readByte() != 0;
			vibrateOnTouch = in.readByte() != 0;
		}
		
		public Volume getVolume() {
			return volume;
		}
		public String getRingtone() {
			return ringtone;
		}
		public boolean isVibrateWhenRing() {
			return vibrateWhenRing;
		}
		public String getNotificationSound() {
			return notificationSound;
		}
		public boolean isDialPadTones() {
			return dialPadTones;
		}
		public boolean isTouchSounds() {
			return touchSounds;
		}
		public boolean isScreenLockSounds() {
			return screenLockSounds;
		}
		public boolean isVibrateOnTouch() {
			return vibrateOnTouch;
		}
		public void setVolume(Volume volume) {
			this.volume = volume;
		}
		public void setRingtone(String ringtone) {
			this.ringtone = ringtone;
		}
		public void setVibrateWhenRing(boolean vibrateWhenRing) {
			this.vibrateWhenRing = vibrateWhenRing;
		}
		public void setNotificationSound(String notificationSound) {
			this.notificationSound = notificationSound;
		}
		public void setDialPadTones(boolean dialPadTones) {
			this.dialPadTones = dialPadTones;
		}
		public void setTouchSounds(boolean touchSounds) {
			this.touchSounds = touchSounds;
		}
		public void setScreenLockSounds(boolean screenLockSounds) {
			this.screenLockSounds = screenLockSounds;
		}
		public void setVibrateOnTouch(boolean vibrateOnTouch) {
			this.vibrateOnTouch = vibrateOnTouch;
		}
		
		public static Sound getInstance() {
			return new Sound();
		}
		
		@Override
		public int describeContents() {
			return 0;
		}
		@Override
		public void writeToParcel(Parcel dest, int flags) {
			dest.writeParcelable(volume, flags);
			dest.writeString(ringtone);
			dest.writeByte((byte) (vibrateWhenRing ? 1 : 0));
			dest.writeString(notificationSound);
			dest.writeByte((byte) (dialPadTones ? 1 : 0));
			dest.writeByte((byte) (touchSounds ? 1 : 0));
			dest.writeByte((byte) (screenLockSounds ? 1 : 0));
			dest.writeByte((byte) (vibrateOnTouch ? 1 : 0));
		}
		
		public static final Parcelable.Creator<Sound> CREATOR = new Parcelable.Creator<Sound>() {

			@Override
			public Sound createFromParcel(Parcel source) {
				return new Sound(source);
			}

			@Override
			public Sound[] newArray(int size) {
				return new Sound[size];
			}
		};
	}
	
	public static class Display implements Parcelable {
		private Brightness brightness;
		private boolean autoRoatate;
		private double sleep;
		private String fontSize;
		
		public Display() {
			
		}
		
		public Display(Parcel in) {
			brightness = (Brightness) in.readParcelable(Brightness.class.getClassLoader());
			autoRoatate = in.readByte() != 0;
			sleep = in.readDouble();
			fontSize = in.readString();
		}

		public Brightness getBrightness() {
			return brightness;
		}
		public boolean isAutoRoatate() {
			return autoRoatate;
		}
		public double getSleep() {
			return sleep;
		}
		public String getFontSize() {
			return fontSize;
		}
		public void setBrightness(Brightness brightness) {
			this.brightness = brightness;
		}
		public void setAutoRoatate(boolean autoRoatate) {
			this.autoRoatate = autoRoatate;
		}
		public void setSleep(double sleep) {
			this.sleep = sleep;
		}
		public void setFontSize(String fontSize) {
			this.fontSize = fontSize;
		}
		
		public static Display getInstance() {
			return new Display();
		}
		
		@Override
		public int describeContents() {
			return 0;
		}
		@Override
		public void writeToParcel(Parcel dest, int flags) {
			dest.writeParcelable(brightness, flags);
			dest.writeByte((byte) (autoRoatate ? 1 : 0));
			dest.writeDouble(sleep);
			dest.writeString(fontSize);
		}
		
		public static final Parcelable.Creator<Display> CREATOR = new Parcelable.Creator<Display>() {

			@Override
			public Display createFromParcel(Parcel source) {
				return new Display(source);
			}

			@Override
			public Display[] newArray(int size) {
				return new Display[size];
			}
		};
	}

	public static class Storage implements Parcelable {
		private double totalSpace;
		private double available;
		private double apps;
		private double pictures;
		private double audio;
		private double downloads;
		private double cachedData;
		private double misc;
		
		public Storage() {
			
		}
		
		public Storage(Parcel in) {
			totalSpace = in.readDouble();
			available = in.readDouble();
			apps = in.readDouble();
			pictures = in.readDouble();
			audio = in.readDouble();
			downloads = in.readDouble();
			cachedData = in.readDouble();
			misc = in.readDouble();
		}

		public double getTotalSpace() {
			return totalSpace;
		}
		public double getAvailable() {
			return available;
		}
		public double getApps() {
			return apps;
		}
		public double getPictures() {
			return pictures;
		}
		public double getAudio() {
			return audio;
		}
		public double getDownloads() {
			return downloads;
		}
		public double getCachedData() {
			return cachedData;
		}
		public double getMisc() {
			return misc;
		}
		public void setTotalSpace(double totalSpace) {
			this.totalSpace = totalSpace;
		}
		public void setAvailable(double available) {
			this.available = available;
		}
		public void setApps(double apps) {
			this.apps = apps;
		}
		public void setPictures(double pictures) {
			this.pictures = pictures;
		}
		public void setAudio(double audio) {
			this.audio = audio;
		}
		public void setDownloads(double downloads) {
			this.downloads = downloads;
		}
		public void setCachedData(double cachedData) {
			this.cachedData = cachedData;
		}
		public void setMisc(double misc) {
			this.misc = misc;
		}
		
		public static Storage getInstance() {
			return new Storage();
		}
		
		@Override
		public int describeContents() {
			return 0;
		}
		@Override
		public void writeToParcel(Parcel dest, int flags) {
			dest.writeDouble(totalSpace);
			dest.writeDouble(available);
			dest.writeDouble(apps);
			dest.writeDouble(pictures);
			dest.writeDouble(audio);
			dest.writeDouble(downloads);
			dest.writeDouble(cachedData);
			dest.writeDouble(misc);
		}
		
		public static final Parcelable.Creator<Storage> CREATOR = new Parcelable.Creator<Storage>() {

			@Override
			public Storage createFromParcel(Parcel source) {
				return new Storage(source);
			}

			@Override
			public Storage[] newArray(int size) {
				return new Storage[size];
			}
		};
	}
	
	public static class Battery implements Parcelable {
	    private String status;
		private double level;
		private double timeOnBattery;
		private List<Stats> stats;

		public Battery() {
			stats = new ArrayList<Stats>();
		}
		
		public Battery(Parcel in) {
			this();
			status = in.readString();
			level = in.readDouble();
			timeOnBattery = in.readDouble();
			in.readTypedList(stats, Stats.CREATOR);
		}

		public String getStatus() {
			return status;
		}
		public double getLevel() {
			return level;
		}
		public double getTimeOnBattery() {
			return timeOnBattery;
		}
		public List<Stats> getStats() {
			return stats;
		}
		public void setStatus(String status) {
			this.status = status;
		}
		public void setLevel(double level) {
			this.level = level;
		}
		public void setTimeOnBattery(double timeOnBattery) {
			this.timeOnBattery = timeOnBattery;
		}
		public void setStats(List<Stats> stats) {
			this.stats = stats;
		}
		
		public static Battery getInstance() {
			return new Battery();
		}
		
		@Override
		public int describeContents() {
			return 0;
		}
		@Override
		public void writeToParcel(Parcel dest, int flags) {
			dest.writeString(status);
			dest.writeDouble(level);
			dest.writeDouble(timeOnBattery);
			dest.writeTypedList(stats);
		}
		
		public static final Parcelable.Creator<Battery> CREATOR = new Parcelable.Creator<Battery>() {

			@Override
			public Battery createFromParcel(Parcel source) {
				return new Battery(source);
			}

			@Override
			public Battery[] newArray(int size) {
				return new Battery[size];
			}
		};
	}
	
	public static class Apps implements Parcelable {
		private List<Application> downloaded;
		private List<Application> running;
		private List<Application> all;
		private Memory internalStorage;
		private Memory ram;

		public Apps() {
			downloaded = new ArrayList<Application>();
			running = new ArrayList<Application>();
			all = new ArrayList<Application>();
		}
		
		public Apps(Parcel in) {
			this();
			in.readTypedList(downloaded, Application.CREATOR);
			in.readTypedList(running, Application.CREATOR);
			in.readTypedList(all, Application.CREATOR);
			internalStorage = in.readParcelable(Memory.class.getClassLoader());
			ram = in.readParcelable(Memory.class.getClassLoader());
		}

		public List<Application> getDownloaded() {
			return downloaded;
		}
		public List<Application> getRunning() {
			return running;
		}
		public List<Application> getAll() {
			return all;
		}
		public void setDownloaded(List<Application> downloaded) {
			this.downloaded = downloaded;
		}
		public void setRunning(List<Application> running) {
			this.running = running;
		}
		public void setAll(List<Application> all) {
			this.all = all;
		}
		public Memory getInternalStorage() {
			return internalStorage;
		}
		public Memory getRam() {
			return ram;
		}
		public void setInternalStorage(Memory internalStorage) {
			this.internalStorage = internalStorage;
		}
		public void setRam(Memory ram) {
			this.ram = ram;
		}
		
		public static Apps getInstance() {
			return new Apps();
		}
		
		@Override
		public int describeContents() {
			return 0;
		}
		@Override
		public void writeToParcel(Parcel dest, int flags) {
			dest.writeTypedList(downloaded);
			dest.writeTypedList(running);
			dest.writeTypedList(all);
			dest.writeParcelable(internalStorage, flags);
			dest.writeParcelable(ram, flags);
		}
		
		public static final Parcelable.Creator<Apps> CREATOR = new Parcelable.Creator<Apps>() {

			@Override
			public Apps createFromParcel(Parcel source) {
				return new Apps(source);
			}

			@Override
			public Apps[] newArray(int size) {
				return new Apps[size];
			}
		};
	}
	
	public static class DateTime implements Parcelable {
		private boolean autoDateTime;
		private boolean autoTimeZone;
		private String date;
		private String time;
		private String timeZone;
		private String timeFormat;
		private String dateFormat;
		
		public DateTime() {
			
		}
		
		public DateTime(Parcel in) {
			autoDateTime = in.readByte() != 0;
			autoTimeZone = in.readByte() != 0;
			date = in.readString();
			time = in.readString();
			timeZone = in.readString();
			timeFormat = in.readString();
			dateFormat = in.readString();
		}

		public boolean isAutoDateTime() {
			return autoDateTime;
		}
		public boolean isAutoTimeZone() {
			return autoTimeZone;
		}
		public String getDate() {
			return date;
		}
		public String getTime() {
			return time;
		}
		public String getTimeZone() {
			return timeZone;
		}
		public String getTimeFormat() {
			return timeFormat;
		}
		public String getDateFormat() {
			return dateFormat;
		}
		public void setAutoDateTime(boolean autoDateTime) {
			this.autoDateTime = autoDateTime;
		}
		public void setAutoTimeZone(boolean autoTimeZone) {
			this.autoTimeZone = autoTimeZone;
		}
		public void setDate(String date) {
			this.date = date;
		}
		public void setTime(String time) {
			this.time = time;
		}
		public void setTimeZone(String timeZone) {
			this.timeZone = timeZone;
		}
		public void setTimeFormat(String timeFormat) {
			this.timeFormat = timeFormat;
		}
		public void setDateFormat(String dateFormat) {
			this.dateFormat = dateFormat;
		}
		
		public static DateTime getInstance() {
			return new DateTime();
		}
		
		@Override
		public int describeContents() {
			return 0;
		}
		@Override
		public void writeToParcel(Parcel dest, int flags) {
			dest.writeByte((byte) (autoDateTime ? 1 : 0));
			dest.writeByte((byte) (autoTimeZone ? 1 : 0));
			dest.writeString(date);
			dest.writeString(time);
			dest.writeString(timeZone);
			dest.writeString(timeFormat);
			dest.writeString(dateFormat);
		}	
		
		public static final Parcelable.Creator<DateTime> CREATOR = new Parcelable.Creator<DateTime>() {

			@Override
			public DateTime createFromParcel(Parcel source) {
				return new DateTime(source);
			}

			@Override
			public DateTime[] newArray(int size) {
				return new DateTime[size];
			}
		};
	}
	
	public static class About implements Parcelable {
		private Status status;
		private String modelNumber;
		private String androidVersion;
		private String basebandVersion;
		private String kernelVersion;
		private String systemVersion;
		private String buildNumber;
		
		public About() {
			
		}
		
		public About(Parcel in) {
			status = (Status) in.readParcelable(Status.class.getClassLoader());
			modelNumber = in.readString();
			androidVersion = in.readString();
			basebandVersion = in.readString();
			kernelVersion = in.readString();
			systemVersion = in.readString();
			buildNumber = in.readString();
		}

		public Status getStatus() {
			return status;
		}
		public String getModelNumber() {
			return modelNumber;
		}
		public String getAndroidVersion() {
			return androidVersion;
		}
		public String getBasebandVersion() {
			return basebandVersion;
		}
		public String getKernelVersion() {
			return kernelVersion;
		}
		public String getSystemVersion() {
			return systemVersion;
		}
		public String getBuildNumber() {
			return buildNumber;
		}
		public void setStatus(Status status) {
			this.status = status;
		}
		public void setModelNumber(String modelNumber) {
			this.modelNumber = modelNumber;
		}
		public void setAndroidVersion(String androidVersion) {
			this.androidVersion = androidVersion;
		}
		public void setBasebandVersion(String basebandVersion) {
			this.basebandVersion = basebandVersion;
		}
		public void setKernelVersion(String kernelVersion) {
			this.kernelVersion = kernelVersion;
		}
		public void setSystemVersion(String systemVersion) {
			this.systemVersion = systemVersion;
		}
		public void setBuildNumber(String buildNumber) {
			this.buildNumber = buildNumber;
		}
		
		public static About getInstance() {
			return new About();
		}
		
		@Override
		public int describeContents() {
			return 0;
		}
		@Override
		public void writeToParcel(Parcel dest, int flags) {
			dest.writeParcelable(status, flags);
			dest.writeString(modelNumber);
			dest.writeString(androidVersion);
			dest.writeString(basebandVersion);
			dest.writeString(kernelVersion);
			dest.writeString(systemVersion);
			dest.writeString(buildNumber);
		}
		
		public static final Parcelable.Creator<About> CREATOR = new Parcelable.Creator<About>() {

			@Override
			public About createFromParcel(Parcel source) {
				return new About(source);
			}

			@Override
			public About[] newArray(int size) {
				return new About[size];
			}
		};
	}
	
	public static class AvailableNetworks implements Parcelable {
		private String name;
		private String status;
		private String signalStrength;
		private String linkSpeed;
		private String security;
		private String ipAddress;
		
		public AvailableNetworks() {
			
		}
		
		public AvailableNetworks(Parcel in) {
			name = in.readString();
			status = in.readString();
			signalStrength = in.readString();
			linkSpeed = in.readString();
			security = in.readString();
			ipAddress = in.readString();
		}

		public String getName() {
			return name;
		}
		public String getStatus() {
			return status;
		}
		public String getSignalStrength() {
			return signalStrength;
		}
		public String getLinkSpeed() {
			return linkSpeed;
		}
		public String getSecurity() {
			return security;
		}
		public String getIpAddress() {
			return ipAddress;
		}
		public void setName(String name) {
			this.name = name;
		}
		public void setStatus(String status) {
			this.status = status;
		}
		public void setSignalStrength(String signalStrength) {
			this.signalStrength = signalStrength;
		}
		public void setLinkSpeed(String linkSpeed) {
			this.linkSpeed = linkSpeed;
		}
		public void setSecurity(String security) {
			this.security = security;
		}
		public void setIpAddress(String ipAddress) {
			this.ipAddress = ipAddress;
		}
		
		public static AvailableNetworks getInstance() {
			return new AvailableNetworks();
		}
		
		@Override
		public int describeContents() {
			return 0;
		}
		@Override
		public void writeToParcel(Parcel dest, int flags) {
			dest.writeString(name);
			dest.writeString(status);
			dest.writeString(signalStrength);
			dest.writeString(linkSpeed);
			dest.writeString(security);
			dest.writeString(ipAddress);
		}
		
		public static final Parcelable.Creator<AvailableNetworks> CREATOR = new Parcelable.Creator<AvailableNetworks>() {

			@Override
			public AvailableNetworks createFromParcel(Parcel source) {
				return new AvailableNetworks(source);
			}

			@Override
			public AvailableNetworks[] newArray(int size) {
				return new AvailableNetworks[size];
			}
		};
	}
	
	public static class PairedDevices implements Parcelable {
		private String name;

		public PairedDevices() {
			
		}
		
		public PairedDevices(Parcel in) {
			name = in.readString();
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public static PairedDevices getInstance() {
			return new PairedDevices();
		}
		
		@Override
		public int describeContents() {
			return 0;
		}

		@Override
		public void writeToParcel(Parcel dest, int flags) {
			dest.writeString(name);
		}
		
		public static final Parcelable.Creator<PairedDevices> CREATOR = new Parcelable.Creator<PairedDevices>() {

			@Override
			public PairedDevices createFromParcel(Parcel source) {
				return new PairedDevices(source);
			}

			@Override
			public PairedDevices[] newArray(int size) {
				return new PairedDevices[size];
			}
		};
	}
	
	public static class Volume implements Parcelable {
		private int music;
		private int ringtone;
		private int alarm;
		private int notification;
		
		public Volume() {
			
		}
		
		public Volume(Parcel in) {
			music = in.readInt();
			ringtone = in.readInt();
			alarm = in.readInt();
			notification = in.readInt();
		}

		public int getMusic() {
			return music;
		}
		public int getRingtone() {
			return ringtone;
		}
		public int getAlarm() {
			return alarm;
		}
		public void setMusic(int music) {
			this.music = music;
		}
		public void setRingtone(int ringtone) {
			this.ringtone = ringtone;
		}
		public void setAlarm(int alarm) {
			this.alarm = alarm;
		}
		public int getNotification() {
			return notification;
		}
		public void setNotification(int notification) {
			this.notification = notification;
		}
		
		public static Volume getInstance() {
			return new Volume();
		}
		
		@Override
		public int describeContents() {
			return 0;
		}
		@Override
		public void writeToParcel(Parcel dest, int flags) {
			dest.writeInt(music);
			dest.writeInt(ringtone);
			dest.writeInt(alarm);
			dest.writeInt(notification);
		}	
		
		public static final Parcelable.Creator<Volume> CREATOR = new Parcelable.Creator<Volume>() {

			@Override
			public Volume createFromParcel(Parcel source) {
				return new Volume(source);
			}

			@Override
			public Volume[] newArray(int size) {
				return new Volume[size];
			}
		};
	}
	
	public static class Brightness implements Parcelable {
		private String mode;
		private int level;
		
		public Brightness() {
			
		}
		
		public Brightness(Parcel in) {
			mode = in.readString();
			level = in.readInt();
		}

		public String getMode() {
			return mode;
		}
		public int getLevel() {
			return level;
		}
		public void setMode(String mode) {
			this.mode = mode;
		}
		public void setLevel(int level) {
			this.level = level;
		}
		
		public static Brightness getInstance() {
			return new Brightness();
		}
		
		@Override
		public int describeContents() {
			return 0;
		}
		@Override
		public void writeToParcel(Parcel dest, int flags) {
			dest.writeString(mode);
			dest.writeInt(level);
		}	
		
		public static final Parcelable.Creator<Brightness> CREATOR = new Parcelable.Creator<Brightness>() {

			@Override
			public Brightness createFromParcel(Parcel source) {
				return new Brightness(source);
			}

			@Override
			public Brightness[] newArray(int size) {
				return new Brightness[size];
			}
		};
	}
	
	public static class Stats implements Parcelable {
		private String type;
		private double percent;
		
		public Stats() {
			
		}
		
		public Stats(Parcel in) {
			type = in.readString();
			percent = in.readDouble();
		}

		public String getType() {
			return type;
		}
		public double getPercent() {
			return percent;
		}
		public void setType(String type) {
			this.type = type;
		}
		public void setPercent(double percent) {
			this.percent = percent;
		}
		
		public static Stats getInstance() {
			return new Stats();
		}
		
		@Override
		public int describeContents() {
			return 0;
		}
		@Override
		public void writeToParcel(Parcel dest, int flags) {
			dest.writeString(type);
			dest.writeDouble(percent);
		}	
		
		public static final Parcelable.Creator<Stats> CREATOR = new Parcelable.Creator<Stats>() {

			@Override
			public Stats createFromParcel(Parcel source) {
				return new Stats(source);
			}

			@Override
			public Stats[] newArray(int size) {
				return new Stats[size];
			}
		};
	}
	
	public static class Application implements Parcelable {
		private String iconUrl;
		private String name;
		private String version;
		private boolean showNotification;
		private double totalSize;
		private double appSize;
		private double data;
		private double cache;	
		private boolean defaults;
		private List<Permissions> permissions;
		private List<Service> services;
		private List<Process> processes;
		
		public Application() {
			permissions = new ArrayList<Permissions>();
			services = new ArrayList<Service>();
			processes = new ArrayList<Process>();
		}
		
		public Application(Parcel in) {
			this();
			iconUrl = in.readString();
			name = in.readString();
			version = in.readString();
			showNotification = in.readByte() != 0;
			totalSize = in.readDouble();
			appSize = in.readDouble();
			data = in.readDouble();
			cache = in.readDouble();
			defaults = in.readByte() != 0;
			in.readTypedList(permissions, Permissions.CREATOR);
			in.readTypedList(services, Service.CREATOR);
			in.readTypedList(processes, Process.CREATOR);
		}

		public String getIconUrl() {
			return iconUrl;
		}

		public void setIconUrl(String iconUrl) {
			this.iconUrl = iconUrl;
		}

		public String getName() {
			return name;
		}
		public String getVersion() {
			return version;
		}
		public boolean isShowNotification() {
			return showNotification;
		}
		public double getTotalSize() {
			return totalSize;
		}
		public double getAppSize() {
			return appSize;
		}
		public double getData() {
			return data;
		}
		public double getCache() {
			return cache;
		}
		public boolean isDefaults() {
			return defaults;
		}
		public List<Permissions> getPermissions() {
			return permissions;
		}
		public List<Service> getServices() {
			return services;
		}
		public List<Process> getProcesses() {
			return processes;
		}
		public void setName(String name) {
			this.name = name;
		}
		public void setVersion(String version) {
			this.version = version;
		}
		public void setShowNotification(boolean showNotification) {
			this.showNotification = showNotification;
		}
		public void setTotalSize(double totalSize) {
			this.totalSize = totalSize;
		}
		public void setAppSize(double appSize) {
			this.appSize = appSize;
		}
		public void setData(double data) {
			this.data = data;
		}
		public void setCache(double cache) {
			this.cache = cache;
		}
		public void setDefaults(boolean defaults) {
			this.defaults = defaults;
		}
		public void setPermissions(List<Permissions> permissions) {
			this.permissions = permissions;
		}
		public void setServices(List<Service> services) {
			this.services = services;
		}
		public void setProcesses(List<Process> processes) {
			this.processes = processes;
		}

		public static Application getInstance() {
			return new Application();
		}
		
		@Override
		public int describeContents() {
			return 0;
		}
		@Override
		public void writeToParcel(Parcel dest, int flags) {
			dest.writeString(iconUrl);
			dest.writeString(name);
			dest.writeString(version);
			dest.writeByte((byte) (showNotification ? 1 : 0));
			dest.writeDouble(totalSize);
			dest.writeDouble(appSize);
			dest.writeDouble(data);
			dest.writeDouble(cache);
			dest.writeByte((byte) (defaults ? 1 : 0));
			dest.writeTypedList(permissions);
			dest.writeTypedList(services);
			dest.writeTypedList(processes);
		}
		
		public static final Parcelable.Creator<Application> CREATOR = new Parcelable.Creator<Application>() {

			@Override
			public Application createFromParcel(Parcel source) {
				return new Application(source);
			}

			@Override
			public Application[] newArray(int size) {
				return new Application[size];
			}
		};
	}
	
	public static class Status implements Parcelable {
		private SIM sim;
		private Battery battery;
		private String ipAddress;
		private String wifiMac;
		private String bluetoothAddress;
		private String serialNumber;
		private double upTime;
		
		public Status() {
			
		}
		
		public Status(Parcel in) {
			sim = (SIM) in.readParcelable(SIM.class.getClassLoader());
			battery = (Battery) in.readParcelable(Battery.class.getClassLoader());
			ipAddress = in.readString();
			wifiMac = in.readString();
			bluetoothAddress = in.readString();
			serialNumber = in.readString();
			upTime = in.readDouble();
		}

		public SIM getSim() {
			return sim;
		}
		public Battery getBattery() {
			return battery;
		}
		public String getIpAddress() {
			return ipAddress;
		}
		public String getWifiMac() {
			return wifiMac;
		}
		public String getBluetoothAddress() {
			return bluetoothAddress;
		}
		public String getSerialNumber() {
			return serialNumber;
		}
		public double getUpTime() {
			return upTime;
		}
		public void setSim(SIM sim) {
			this.sim = sim;
		}
		public void setBattery(Battery battery) {
			this.battery = battery;
		}
		public void setIpAddress(String ipAddress) {
			this.ipAddress = ipAddress;
		}
		public void setWifiMac(String wifiMac) {
			this.wifiMac = wifiMac;
		}
		public void setBluetoothAddress(String bluetoothAddress) {
			this.bluetoothAddress = bluetoothAddress;
		}
		public void setSerialNumber(String serialNumber) {
			this.serialNumber = serialNumber;
		}
		public void setUpTime(double upTime) {
			this.upTime = upTime;
		}
		
		public static Status getInstance() {
			return new Status();
		}
		
		@Override
		public int describeContents() {
			return 0;
		}
		@Override
		public void writeToParcel(Parcel dest, int flags) {
			dest.writeParcelable(sim, flags);
			dest.writeParcelable(battery, flags);
			dest.writeString(ipAddress);
			dest.writeString(wifiMac);
			dest.writeString(bluetoothAddress);
			dest.writeString(serialNumber);
			dest.writeDouble(upTime);
		}	
		
		public static final Parcelable.Creator<Status> CREATOR = new Parcelable.Creator<Status>() {

			@Override
			public Status createFromParcel(Parcel source) {
				return new Status(source);
			}

			@Override
			public Status[] newArray(int size) {
				return new Status[size];
			}
		};
	}
	
	public static class Memory implements Parcelable {
		private double used;
		private double free;
		
		public Memory() {
			
		}
		
		public Memory(Parcel in) {
			used = in.readDouble();
			free = in.readDouble();
		}

		public double getUsed() {
			return used;
		}
		public double getFree() {
			return free;
		}
		public void setUsed(double used) {
			this.used = used;
		}
		public void setFree(double free) {
			this.free = free;
		}
		
		public static Memory getInstance() {
			return new Memory();
		}
		
		@Override
		public int describeContents() {
			return 0;
		}
		@Override
		public void writeToParcel(Parcel dest, int flags) {
			dest.writeDouble(used);
			dest.writeDouble(free);
		}	
		
		public static final Parcelable.Creator<Memory> CREATOR = new Parcelable.Creator<Memory>() {

			@Override
			public Memory createFromParcel(Parcel source) {
				return new Memory(source);
			}

			@Override
			public Memory[] newArray(int size) {
				return new Memory[size];
			}
		};
	}
	
	public static class Permissions implements Parcelable {
		private String type;
		private String description;
		
		public Permissions() {
			
		}
		
		public Permissions(Parcel in) {
			type = in.readString();
			description = in.readString();
		}

		public String getType() {
			return type;
		}
		public String getDescription() {
			return description;
		}
		public void setType(String type) {
			this.type = type;
		}
		public void setDescription(String description) {
			this.description = description;
		}
		
		public static Permissions getInstance() {
			return new Permissions();
		}
		
		@Override
		public int describeContents() {
			return 0;
		}
		@Override
		public void writeToParcel(Parcel dest, int flags) {
			dest.writeString(type);
			dest.writeString(description);
		}	
		
		public static final Parcelable.Creator<Permissions> CREATOR = new Parcelable.Creator<Permissions>() {

			@Override
			public Permissions createFromParcel(Parcel source) {
				return new Permissions(source);
			}

			@Override
			public Permissions[] newArray(int size) {
				return new Permissions[size];
			}
		};
	}
	
	public static class Service implements Parcelable {
		private String name;
		private String startedBy;
		
		public Service() {
			
		}
		
		public Service(Parcel in) {
			name = in.readString();
			startedBy = in.readString();
		}

		public String getName() {
			return name;
		}
		public String getStartedBy() {
			return startedBy;
		}
		public void setName(String name) {
			this.name = name;
		}
		public void setStartedBy(String startedBy) {
			this.startedBy = startedBy;
		}
		
		public static Service getInstance() {
			return new Service();
		}
		
		@Override
		public int describeContents() {
			return 0;
		}
		@Override
		public void writeToParcel(Parcel dest, int flags) {
			dest.writeString(name);
			dest.writeString(startedBy);
		}
		
		public static final Parcelable.Creator<Service> CREATOR = new Parcelable.Creator<Service>() {

			@Override
			public Service createFromParcel(Parcel source) {
				return new Service(source);
			}

			@Override
			public Service[] newArray(int size) {
				return new Service[size];
			}
		};
	}
	
	public static class Process implements Parcelable {
		private String name;
		private String appPackage;
		
		public Process() {
			
		}
		
		public Process(Parcel in) {
			name = in.readString();
			appPackage = in.readString();
		}

		public String getName() {
			return name;
		}
		public String getAppPackage() {
			return appPackage;
		}
		public void setName(String name) {
			this.name = name;
		}
		public void setAppPackage(String appPackage) {
			this.appPackage = appPackage;
		}
		
		public static Process getInstance() {
			return new Process();
		}
		
		@Override
		public int describeContents() {
			return 0;
		}
		@Override
		public void writeToParcel(Parcel dest, int flags) {
			dest.writeString(name);
			dest.writeString(appPackage);
		}	
		
		public static final Parcelable.Creator<Process> CREATOR = new Parcelable.Creator<Process>() {

			@Override
			public Process createFromParcel(Parcel source) {
				return new Process(source);
			}

			@Override
			public Process[] newArray(int size) {
				return new Process[size];
			}
		};
	}
	
	public static class SIM implements Parcelable {
		private String network;
		private String signalStrength;
		private String mobileNetworkType;
		private String serviceStatus;
		private String roaming;
		private String mobileNetworkState;
		private String phoneNumber;
		private String imei;
		private String imeiSv;
		
		public SIM() {
			
		}
		
		public SIM(Parcel in) {
			network = in.readString();
			signalStrength = in.readString();
			mobileNetworkType = in.readString();
			serviceStatus = in.readString();
			roaming = in.readString();
			mobileNetworkState = in.readString();
			phoneNumber = in.readString();
			imei = in.readString();
			imeiSv = in.readString();
		}

		public String getNetwork() {
			return network;
		}
		public String getSignalStrength() {
			return signalStrength;
		}
		public String getMobileNetworkType() {
			return mobileNetworkType;
		}
		public String getServiceStatus() {
			return serviceStatus;
		}
		public String getRoaming() {
			return roaming;
		}
		public String getMobileNetworkState() {
			return mobileNetworkState;
		}
		public String getPhoneNumber() {
			return phoneNumber;
		}
		public String getImei() {
			return imei;
		}
		public String getImeiSv() {
			return imeiSv;
		}
		public void setNetwork(String network) {
			this.network = network;
		}
		public void setSignalStrength(String signalStrength) {
			this.signalStrength = signalStrength;
		}
		public void setMobileNetworkType(String mobileNetworkType) {
			this.mobileNetworkType = mobileNetworkType;
		}
		public void setServiceStatus(String serviceStatus) {
			this.serviceStatus = serviceStatus;
		}
		public void setRoaming(String roaming) {
			this.roaming = roaming;
		}
		public void setMobileNetworkState(String mobileNetworkState) {
			this.mobileNetworkState = mobileNetworkState;
		}
		public void setPhoneNumber(String phoneNumber) {
			this.phoneNumber = phoneNumber;
		}
		public void setImei(String imei) {
			this.imei = imei;
		}
		public void setImeiSv(String imeiSv) {
			this.imeiSv = imeiSv;
		}
		
		public static SIM getInstance() {
			return new SIM();
		}
		
		@Override
		public int describeContents() {
			return 0;
		}
		@Override
		public void writeToParcel(Parcel dest, int flags) {
			dest.writeString(network);
			dest.writeString(signalStrength);
			dest.writeString(mobileNetworkType);
			dest.writeString(serviceStatus);
			dest.writeString(roaming);
			dest.writeString(mobileNetworkState);
			dest.writeString(phoneNumber);
			dest.writeString(imei);
			dest.writeString(imeiSv);
		}	
		
		public static final Parcelable.Creator<SIM> CREATOR = new Parcelable.Creator<SIM>() {

			@Override
			public SIM createFromParcel(Parcel source) {
				return new SIM(source);
			}

			@Override
			public SIM[] newArray(int size) {
				return new SIM[size];
			}
		};
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeParcelable(wifi, flags);
		dest.writeParcelable(flight, flags);
		dest.writeParcelable(bluetooth, flags);
		dest.writeParcelable(sound, flags);
		dest.writeParcelable(display, flags);
		dest.writeParcelable(storage, flags);
		dest.writeParcelable(battery, flags);
		dest.writeParcelable(apps, flags);
		dest.writeParcelable(dateTime, flags);
		dest.writeParcelable(about, flags);
	}
	
	public static final Parcelable.Creator<RemoteSettings> CREATOR = new Parcelable.Creator<RemoteSettings>() {

		@Override
		public RemoteSettings createFromParcel(Parcel source) {
			return new RemoteSettings(source);
		}

		@Override
		public RemoteSettings[] newArray(int size) {
			return new RemoteSettings[size];
		}
	};
}