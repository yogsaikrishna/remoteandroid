package com.romotoapp.interfaces;

import java.util.List;

import android.graphics.Bitmap;


public interface OnFileDownloadListener {
	public void onFileDownload(String jsonString);
	public void onFileDownload(List<Bitmap> bitmaps);
}
