package com.romotoapp.interfaces;

import org.json.JSONObject;

public interface OnDataSyncListener {
	void onSyncComplete(JSONObject jsonObj);
}
