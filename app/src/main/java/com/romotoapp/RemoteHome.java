package com.romotoapp;

import java.util.Vector;

import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.romotoapp.common.ListAdapter;
import com.romotoapp.common.RowData;
import com.romotoapp.constants.Constants;
import com.romotoapp.constants.SessionConstants;
import com.romotoapp.helpers.AlertDialogHelper;
import com.romotoapp.helpers.DBHelper;
import com.romotoapp.interfaces.OnDataSyncListener;
import com.romotoapp.utils.DataSyncTask;
import com.romotoapp.utils.Utils;

public class RemoteHome extends BaseActivity implements OnDataSyncListener, OnItemClickListener {
	private static final String TAG = "RemoteHome";
	
	private String[] listText = { "Devices", "Settings", "Help" };
	private Integer[] listImages = { R.drawable.ic_action_phone, R.drawable.ic_action_settings,
			R.drawable.ic_action_help};
	private DrawerLayout drawer;
	private ActionBarDrawerToggle drawerToggle;
	private ListView listView;
	private Vector<RowData> data;
	private RowData rowData;
	private ListAdapter listAdapter;
	
	private TextView tvUserKey;
	private Button btConnect, btClear;
	private EditText etPidOne, etPidTwo, etPidThree;
	private DataSyncTask dataSyncTask;
	private ProgressDialog progressDialog;
	private Thread thread;
	private String userKey;
	private DBHelper dbHelper;
	private int deviceCount;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.remote_home);
				
		drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
		drawerToggle = new ActionBarDrawerToggle(this, drawer, R.drawable.ic_navigation_drawer, 
				R.string.app_name, R.string.app_name);
		drawer.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
		drawer.setDrawerListener(drawerToggle);
		
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
		
		tvUserKey = (TextView) findViewById(R.id.tv_userkey);
		btConnect = (Button) findViewById(R.id.bt_connect);
		btClear = (Button) findViewById(R.id.bt_clear);
		etPidOne = (EditText) findViewById(R.id.et_pid_one);
		etPidTwo = (EditText) findViewById(R.id.et_pid_two);
		etPidThree = (EditText) findViewById(R.id.et_pid_three);
		
		userKey = Utils.getPrefString(RemoteHome.this, Constants.PROPERTY_USER_KEY);
		tvUserKey.setText(splitUserKey(userKey));
		Log.d(TAG, userKey);
		
		btConnect.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String pidOne = etPidOne.getText().toString();
				String pidTwo = etPidTwo.getText().toString();
				String pidThree = etPidThree.getText().toString();
				
				if (pidOne.equals("")
						|| pidTwo.equals("")
						|| pidThree.equals("")) {
					Utils.makeToast(getApplicationContext(), "Please enter partner ID", Toast.LENGTH_SHORT);
				} else {
					if (Utils.isNetworkAvailable(RemoteHome.this)) {
						etPidOne.setText("");
						etPidTwo.setText("");
						etPidThree.setText("");
						etPidOne.requestFocus();
						
						dataSyncTask = new DataSyncTask(RemoteHome.this);
						dataSyncTask.execute(Constants.URL_COMMAND
								+ "sender_key=" + userKey
								+ "&user_key=" + (pidOne + pidTwo + pidThree)
								+ "&command=" + "connect");
						progressDialog = ProgressDialog.show(RemoteHome.this, "", "Connecting...");						
						SessionConstants.clear();
						SessionConstants.isSessionStarted = true;
					} else {
						Utils.makeToast(getApplicationContext(), 
								Constants.WARN_NO_NETWORK, Toast.LENGTH_SHORT);
					}
				}
			}
		});
		
		btClear.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				etPidOne.setText("");
				etPidTwo.setText("");
				etPidThree.setText("");
				etPidOne.requestFocus();
			}
		});
		
		etPidOne.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
				if (etPidOne.getText().toString().length() == 3) {
					etPidTwo.requestFocus();
				}
			}
			
			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				
			}
			
			@Override
			public void afterTextChanged(Editable arg0) {
				
			}
		});
		
		etPidTwo.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				if (etPidTwo.getText().toString().length() == 3) {
					etPidThree.requestFocus();
				}
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				
			}
		});
		
	}
	
	private String splitUserKey(String userKey) {
		char[] array = userKey.toCharArray();
		StringBuffer key = new StringBuffer();
		for (int i = 0; i < array.length; i++) {
			if (i == 2 
				|| i == 5) {
				key.append(array[i]);
				key.append(" ");
			} else {
				key.append(array[i]);
			}
		}
		return key.toString();
	}

	@Override
	public void onSyncComplete(JSONObject jsonObj) {
		progressDialog.dismiss();
		progressDialog = ProgressDialog.show(RemoteHome.this, "", "Waiting for approval...");
		thread = new Thread(new Runnable() {
	
			@Override
			public void run() {
				try {
					for (int count = 0; count < 10; count ++) {
						Thread.sleep(1000);
						if (SessionConstants.isRemoteAccept
								|| SessionConstants.isRemoteReject) {
							progressDialog.dismiss();
							thread.interrupt();
						}
					}
					progressDialog.dismiss();
					if (!SessionConstants.isRemoteReject
							&& !SessionConstants.isRemoteAccept) {
						Intent intent = new Intent(RemoteHome.this, AlertDialogHelper.class);
						intent.putExtra("sender_key", userKey);
						intent.putExtra("command", "alert");
						startActivity(intent);			
					}
				} catch (InterruptedException e) {
					Log.e(TAG, "Exception occured: " + e.getMessage());
				}
			}
		});
		thread.start();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		drawerToggle.onConfigurationChanged(newConfig);
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		drawerToggle.syncState();	
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (drawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View view, int position, long id) {
		switch (position) {
		case 1:
			Intent intent = new Intent(RemoteHome.this, RemoteDevices.class);
			startActivity(intent);
			drawer.closeDrawers();
			break;
		case 2:
			break;
		case 3:
			break;
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		dbHelper = new DBHelper(this);
		deviceCount = dbHelper.getSavedUserCount();
		dbHelper.close();
		listView = (ListView) findViewById(R.id.left_drawer);
		data = new Vector<RowData>();
		for (int count = -1; count < listText.length; count++) {
			if (count < 0) {
				rowData = new RowData(null, -1, null);
			} else if (deviceCount >= 0) {
				rowData = new RowData(listText[count], listImages[count], "" + deviceCount);
				deviceCount = -1;
			} else {
				rowData = new RowData(listText[count], listImages[count], null);
			}
			data.add(rowData);
		}
		listAdapter = new ListAdapter(this, R.layout.drawer_list_item,
				R.id.tv_text, data);
		listView.setAdapter(listAdapter);
		listView.setOnItemClickListener(this);
	}
}
