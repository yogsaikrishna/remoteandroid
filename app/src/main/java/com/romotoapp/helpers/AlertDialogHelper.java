package com.romotoapp.helpers;

import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;

import com.romotoapp.common.RemoteUser;
import com.romotoapp.constants.Constants;
import com.romotoapp.enums.Command;
import com.romotoapp.interfaces.OnDataSyncListener;
import com.romotoapp.services.SettingsService;
import com.romotoapp.utils.CommandUtils;
import com.romotoapp.utils.DataSyncTask;
import com.romotoapp.utils.Utils;

public class AlertDialogHelper extends Activity implements OnDataSyncListener {
	private DataSyncTask dataSyncTask;
	private AlertDialog.Builder alertDialog;
	private Bundle bundle;
	private String command;
	private RemoteUser remoteUser;
	private DBHelper db;
	private boolean isUserExists;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		bundle = getIntent().getExtras();
		db = new DBHelper(this);
		if (bundle != null) {
			remoteUser = new RemoteUser();
			remoteUser.setSenderId(bundle.getString("sender_key"));
			remoteUser.setSenderName(bundle.getString("sender"));
			remoteUser.setSenderGcmId(bundle.getString("sender_id"));
			remoteUser.setModel(bundle.getString("model"));			
		}
		isUserExists = db.isUserExists(remoteUser.getSenderId());
		command = CommandUtils.getCommand(bundle);
		String message = bundle.getString("message");
		alertDialog = new AlertDialog.Builder(this);
		if (message == null) {
			bundle.putBoolean("isUserExists", isUserExists);
			alertDialog.setMessage(CommandUtils.getMessage(bundle));			
		} else {
			alertDialog.setMessage(message);
		}
		if (command.equals(Command.CONNECT.getCommand())) {
			alertConnect();			
		} else if (command.equals(Command.ACCEPT.getCommand())){
			alertAccept();
		} else if (command.equals(Command.REJECT.getCommand())){
			if (!isUserExists)
				alertReject();
			else
				alertOther();
		} else {
			alertOther();
		}
		alertDialog.setCancelable(false);
		alertDialog.show();
		Utils.notification(this);
	}

	@Override
	public void onSyncComplete(JSONObject jsonObj) {
		finish();
	}
	
	private void alertConnect() {
		alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				String cmd = Command.REJECT.getCommand();
				dataSyncTask = new DataSyncTask(AlertDialogHelper.this);
				dataSyncTask.execute(Constants.URL_COMMAND
						+ "sender_key=" + Utils.getPrefString(AlertDialogHelper.this,
								Constants.PROPERTY_USER_KEY)
						+ "&user_key=" + remoteUser.getSenderId()
						+ "&command=" + cmd);
				dialog.cancel();
				AlertDialogHelper.this.finish();
			}
		});
		alertDialog.setPositiveButton("Yes", new OnClickListener() {
				
			@Override
			public void onClick(DialogInterface dialog, int which) {
				String cmd = Command.ACCEPT.getCommand();
				dataSyncTask = new DataSyncTask(AlertDialogHelper.this);
				dataSyncTask.execute(Constants.URL_COMMAND
						+ "sender_key=" + Utils.getPrefString(AlertDialogHelper.this,
								Constants.PROPERTY_USER_KEY)
						+ "&user_key=" + remoteUser.getSenderId()
						+ "&command=" + cmd);
				dialog.cancel();
				Intent intent = new Intent(AlertDialogHelper.this, SettingsService.class);
				intent.putExtra("user_key", remoteUser.getSenderId());
				startService(intent);
				AlertDialogHelper.this.finish();
			}
		});
	}
	
	private void alertAccept() {
		if (!isUserExists) {
			alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.cancel();
//					Intent intent = new Intent(AlertDialogHelper.this, RemoteAction.class);
//					intent.putExtras(bundle);
//					startActivity(intent);
					AlertDialogHelper.this.finish();
				}
			});		
			alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.cancel();
					if (remoteUser != null) {
						db.saveRemoteuser(remoteUser);	
					}
					db.close();
//					Intent intent = new Intent(AlertDialogHelper.this, RemoteAction.class);
//					intent.putExtras(bundle);
//					startActivity(intent);
					AlertDialogHelper.this.finish();
				}
			});					
		} else {
			alertDialog.setNeutralButton("OK", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.cancel();
//					Intent intent = new Intent(AlertDialogHelper.this, RemoteAction.class);
//					intent.putExtras(bundle);
//					startActivity(intent);
					AlertDialogHelper.this.finish();
				}
			});		
		}
	}
	
	private void alertReject() {
		alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
				AlertDialogHelper.this.finish();
			}
		});		
		alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
				if (remoteUser != null) {
					db.saveRemoteuser(remoteUser);
				}
				db.close();
				AlertDialogHelper.this.finish();
			}
		});		
	}
	
	private void alertOther() {
		alertDialog.setNeutralButton("OK", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
				AlertDialogHelper.this.finish();
			}
		});				
	}
}
