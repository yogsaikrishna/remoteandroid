package com.romotoapp.helpers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.romotoapp.common.RemoteUser;

public class DBHelper extends SQLiteOpenHelper {
	private static final String TAG = "DBHelper";
	
	private static final int DB_VERSION = 1;
	private static final String DB_NAME = "remoteAndroid";
	private static final String TABLE_DEVICES = "devices";
	private static final String KEY_ID = "id";
	private static final String KEY_SENDER_ID = "sender_id";
	private static final String KEY_SENDER_NAME = "sender_name";
	private static final String KEY_SENDER_REGID = "sender_regId";
	private static final String KEY_DEVICE_MODEL = "model";
	private static final String KEY_CREATED_DATE = "created_date";
	
	private static final String CREATE_TABLE = "CREATE TABLE "
			+ TABLE_DEVICES + "(" + KEY_ID + " INTEGER PRIMARY KEY," + KEY_SENDER_ID
			+ " Text," + KEY_SENDER_NAME + " Text, " + KEY_SENDER_REGID + " Text,"
			+ KEY_DEVICE_MODEL + " Text," + KEY_CREATED_DATE + " DATETIME" + ")";

	public DBHelper(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(CREATE_TABLE);
		Log.d(TAG, "Table created");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_DEVICES);
		onCreate(db);
	}
	
	public long saveRemoteuser(RemoteUser remoteUser) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(KEY_SENDER_ID, remoteUser.getSenderId());
		values.put(KEY_SENDER_NAME, remoteUser.getSenderName());
		values.put(KEY_SENDER_REGID, remoteUser.getSenderGcmId());
		values.put(KEY_DEVICE_MODEL, remoteUser.getModel());
		values.put(KEY_CREATED_DATE, getCreatedDate());
		long id = db.insert(TABLE_DEVICES, null, values);
		return id;
	}

	public RemoteUser getRemoteUser(long id) {
		SQLiteDatabase db = this.getReadableDatabase();
		String query = "SELECT * FROM " + TABLE_DEVICES + " WHERE "
				+ KEY_ID + " = " + id;
		Cursor cursor = db.rawQuery(query, null);
		if (cursor != null) {
			cursor.moveToFirst();
		}
		RemoteUser remoteUser = new RemoteUser();
		remoteUser.setId(cursor.getInt(cursor.getColumnIndex(KEY_ID)));
		remoteUser.setSenderId(cursor.getString(cursor.getColumnIndex(KEY_SENDER_ID)));
		remoteUser.setSenderName(cursor.getString(cursor.getColumnIndex(KEY_SENDER_NAME)));
		remoteUser.setSenderGcmId(cursor.getString(cursor.getColumnIndex(KEY_SENDER_REGID)));
		remoteUser.setModel(cursor.getString(cursor.getColumnIndex(KEY_DEVICE_MODEL)));
		remoteUser.setCreated(cursor.getString(cursor.getColumnIndex(KEY_CREATED_DATE)));
		cursor.close();
		return remoteUser;
	}
	
	public List<RemoteUser> getAllUsers() {
		List<RemoteUser> remoteUsers = new ArrayList<RemoteUser>();
		SQLiteDatabase db = this.getReadableDatabase();
		String query = "SELECT * FROM " + TABLE_DEVICES;
		Cursor cursor = db.rawQuery(query, null);
		if (cursor.moveToFirst()) {
			do {
				RemoteUser remoteUser = new RemoteUser();
				remoteUser.setId(cursor.getInt(cursor.getColumnIndex(KEY_ID)));
				remoteUser.setSenderId(cursor.getString(cursor.getColumnIndex(KEY_SENDER_ID)));
				remoteUser.setSenderName(cursor.getString(cursor.getColumnIndex(KEY_SENDER_NAME)));
				remoteUser.setSenderGcmId(cursor.getString(cursor.getColumnIndex(KEY_SENDER_REGID)));
				remoteUser.setModel(cursor.getString(cursor.getColumnIndex(KEY_DEVICE_MODEL)));
				remoteUser.setCreated(cursor.getString(cursor.getColumnIndex(KEY_CREATED_DATE)));
				remoteUsers.add(remoteUser);
			} while (cursor.moveToNext());
		}
		cursor.close();
		return remoteUsers;
	}
	
	public int getSavedUserCount() {
		SQLiteDatabase db = this.getReadableDatabase();
		String query = "SELECT * FROM " + TABLE_DEVICES;
		Cursor cursor = db.rawQuery(query, null);
		int count = cursor.getCount();
		cursor.close();
		return count;
	}
	
	public void deleteRemoteUser(long id) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_DEVICES, KEY_ID + " = ?", 
				new String[] { String.valueOf(id) });
	}
	
	public void closeDB() {
		SQLiteDatabase db = this.getReadableDatabase();
		if (db != null
				&& db.isOpen()) {
			db.close();
		}
	}
	
	private String getCreatedDate() {
		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"EEEE, MMMM d, yyyy", Locale.getDefault());
		Date date = new Date();
		return dateFormat.format(date);
	}
	
	public boolean isUserExists(String userKey) {
		SQLiteDatabase db = this.getReadableDatabase();
		String query = "SELECT * FROM " + TABLE_DEVICES + " WHERE "
				+ KEY_SENDER_ID + " = " + userKey;
		Cursor cursor = db.rawQuery(query, null);
		if (cursor != null 
				&& cursor.getCount() > 0) {
			return true;
		}
		return false;
	}
}
