package com.romotoapp.utils;

import com.romotoapp.enums.Command;

import android.os.Bundle;

public class CommandUtils {
	
	public static String getSender(Bundle bundle) {
		return bundle.getString("sender");
	}
	
	public static String getCommand(Bundle bundle) {
		return bundle.getString("command");
	}
	
	public static String getSenderKey(Bundle bundle) {
		return bundle.getString("sender_key");
	}
	
	public static String getMessage(Bundle bundle) {
		String message = null;
		String sender = getSender(bundle);
		boolean userExists = bundle.getBoolean("isUserExists");
		String cmd = getCommand(bundle);
		Command command = Command.fromString(cmd);
		switch (command) {
		case ALERT:
			message = "Your request has been sent and waiting for approval";
			break;
		case ACCEPT:
			if (!userExists) 
				message = sender + " accepted your request. Do you want to save the device?";
			else 
				message = sender + " accepted your request";
			break;
		case AIRPLANE_OFF:
			break;
		case AIRPLANE_ON:
			break;
		case BLUETOOTH_OFF:
			break;
		case BLUETOOTH_ON:
			break;
		case CONNECT:
			message = sender + " wants to connect to your mobile";
			break;
		case REJECT:
			if (!userExists) 
				message = sender + " rejected your request. Do you want to save the device?";
			else 
				message = sender + " rejected your request";
			break;
		case WIFI_OFF:
			break;
		case WIFI_ON:
			break;
		default:
			break;
		}
		return message;
	}
}
