package com.romotoapp.utils;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.romotoapp.helpers.AlertDialogHelper;

public class CommandActions {
	public static void alert(Context context, Bundle bundle) {
		Intent intent = new Intent(context, AlertDialogHelper.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.putExtras(bundle);
		context.startActivity(intent);
	}
}
