package com.romotoapp.utils;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import com.romotoapp.constants.Constants;
import com.romotoapp.interfaces.OnFileDownloadListener;

public class FileUtils {
	private static final String TAG = "FileUtils";

	public static File saveToFile(Context context, byte[] fileContents,
			String fileType) {
		File file = null;
		String fileName = fileDigest(fileContents) + "." + fileType;
		try {
			file = context.getFileStreamPath(fileName);
			if (!file.exists()) { 
				FileOutputStream outStream = context.openFileOutput(fileName,
						Context.MODE_PRIVATE);
				outStream.write(fileContents);
				outStream.close();
				file = context.getFileStreamPath(fileName);
			}
		} catch (Exception e) {
			Log.e(TAG, "Exception in saveToFile(+): " + e.getMessage());
		}
		return file;
	}

	public static boolean deleteFile(Context context, String fileName) {
		return context.deleteFile(fileName);
	}

	public static String fileDigest(byte[] fileContents) {
		StringBuffer fileDigest = null;
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(fileContents);
			byte messageDigest[] = md.digest();
			fileDigest = new StringBuffer();
			for (int i = 0; i < messageDigest.length; i++) {
				fileDigest.append(Integer.toHexString(0xFF & messageDigest[i]));
			}
		} catch (Exception e) {
			Log.e(TAG, "Exception in fileDigest(+): " + e.getMessage());
		}
		return fileDigest.toString();
	}

	public static void uploadFile(Context context, final File[] files,
			final String userKey, final String senderKey) {
		new AsyncTask<Void, Void, String>() {
			HttpURLConnection conn = null;
			DataOutputStream dos = null;
			String lineEnd = "\r\n";
			String twoHyphens = "--";
			String boundary = "*****";
			int bytesRead, bytesAvailable, bufferSize;
			byte[] buffer;
			int maxBufferSize = 1 * 1024 * 1024;
			String urlParameters = "user_key=" + userKey + "&sender_key="
					+ senderKey;

			@Override
			protected String doInBackground(Void... params) {
				String message = null;
				try {
					for (File file : files) {
						if (!file.exists()) {
							message = "Exception occured: File does not exists!";
							return message;
						} else {
							FileInputStream fileInputStream = new FileInputStream(
									file);
							URL url = new URL(Constants.URL_UPLOAD
									+ urlParameters);

							conn = (HttpURLConnection) url.openConnection();
							conn.setDoInput(true);
							conn.setDoOutput(true);
							conn.setUseCaches(false);
							conn.setRequestMethod("POST");
							conn.setRequestProperty("Connection", "Keep-Alive");
							conn.setRequestProperty("ENCTYPE",
									"multipart/form-data");
							conn.setRequestProperty("Content-Type",
									"multipart/form-data;boundary=" + boundary);
							conn.setRequestProperty("rm_file", file.getName());

							dos = new DataOutputStream(conn.getOutputStream());
							dos.writeBytes(twoHyphens + boundary + lineEnd);
							dos.writeBytes("Content-Disposition: form-data; name=\"rm_file\";filename=\""
									+ file.getName() + "\"" + lineEnd);

							dos.writeBytes(lineEnd);

							bytesAvailable = fileInputStream.available();

							bufferSize = Math
									.min(bytesAvailable, maxBufferSize);
							buffer = new byte[bufferSize];

							bytesRead = fileInputStream.read(buffer, 0,
									bufferSize);

							while (bytesRead > 0) {

								dos.write(buffer, 0, bufferSize);
								bytesAvailable = fileInputStream.available();
								bufferSize = Math.min(bytesAvailable,
										maxBufferSize);
								bytesRead = fileInputStream.read(buffer, 0,
										bufferSize);

							}

							dos.writeBytes(lineEnd);
							dos.writeBytes(twoHyphens + boundary + twoHyphens
									+ lineEnd);

							int responseCode = conn.getResponseCode();
							String responseMessage = conn.getResponseMessage();

							Log.i(TAG, "HTTP Response is : " + responseMessage
									+ ": " + responseCode);

							fileInputStream.close();
							dos.flush();
							dos.close();
						}
					}

				} catch (Exception e) {
					message = "Exception in uploadFile(+): " + e.getMessage();
				}
				return message;
			}

			@Override
			protected void onPostExecute(String message) {
				super.onPostExecute(message);
				Log.i(TAG, "" + message);
			}
		}.execute(null, null, null);
	}

	public static void downloadFile(final OnFileDownloadListener listener,
			final String fileName) {
		AsyncTask<Void, Void, Void> fileDownload = new AsyncTask<Void, Void, Void>() {
			String jsonString;

			@Override
			protected Void doInBackground(Void... params) {
				try {
					URL url = new URL(Constants.URL_DOWNLOAD + fileName);
					InputStream inputStream = url.openStream();
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(inputStream));
					int ch;
					StringBuilder sb = new StringBuilder();
					while ((ch = reader.read()) != -1) {
						sb.append((char) ch);
					}
					reader.close();
					inputStream.close();
					jsonString = sb.toString();
				} catch (Exception e) {
					Log.i(TAG, "Exception in downloadFile(+): " + e.getMessage());
				}
				return null;
			}

			@Override
			protected void onPostExecute(Void result) {
				super.onPostExecute(result);
				listener.onFileDownload(jsonString);
			}
		};
		fileDownload.execute(null, null, null);
	}

	public static void downloadFile(final OnFileDownloadListener listener,
			final String[] fileNames) {
		AsyncTask<Void, Void, Void> fileDownload = new AsyncTask<Void, Void, Void>() {
			List<Bitmap> bitmapList;

			@Override
			protected Void doInBackground(Void... params) {
				try {
					bitmapList = new ArrayList<Bitmap>();
					for (String fileName : fileNames) {
						HttpGet request = new HttpGet(Constants.URL_DOWNLOAD
								+ fileName);
						HttpClient client = new DefaultHttpClient();
						HttpResponse response = (HttpResponse) client
								.execute(request);
						HttpEntity entity = response.getEntity();
						BufferedHttpEntity bufferedEntity = new BufferedHttpEntity(
								entity);
						InputStream inputStream = bufferedEntity.getContent();
						Bitmap tmp = BitmapFactory.decodeStream(inputStream);
						bitmapList.add(tmp);
						inputStream.close();
					}
				} catch (Exception e) {
					Log.i(TAG, "Exception in downloadFile(+): " + e.getMessage());
				}
				return null;
			}

			@Override
			protected void onPostExecute(Void result) {
				super.onPostExecute(result);
				listener.onFileDownload(bitmapList);
			}
		};
		fileDownload.execute(null, null, null);
	}

	public static byte[] convertBitmapToArray(Bitmap bitmap) {
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
		return stream.toByteArray();
	}
}
