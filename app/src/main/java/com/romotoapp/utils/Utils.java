package com.romotoapp.utils;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Pattern;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.DisplayMetrics;
import android.util.Patterns;
import android.widget.Toast;

import com.romotoapp.R;
import com.romotoapp.RemoteHome;
import com.romotoapp.constants.Constants;

public class Utils {
	public static void makeToast(Context context, String message, int duration) {
		Toast.makeText(context, message, duration).show();
	}

	public static String getUserEmail(Context context) {
		Pattern emailPattern = Patterns.EMAIL_ADDRESS;
		Account[] accounts = AccountManager.get(context).getAccounts();
		for (Account account : accounts) {
			if (emailPattern.matcher(account.name).matches()) {
				return account.name;
			}
		}
		return null;
	}

	public static SharedPreferences getPreferences(Context context) {
		return context.getSharedPreferences(Constants.PREFERENCE_NAME,
				Context.MODE_PRIVATE);
	}

	public static String getPrefString(Context context, String key) {
		SharedPreferences prefs = getPreferences(context);
		String value = prefs.getString(key, null);
		return value;
	}

	public static Boolean getPrefBoolean(Context context, String key) {
		SharedPreferences prefs = getPreferences(context);
		Boolean value = prefs.getBoolean(key, false);
		return value;
	}

	public static int getPrefInt(Context context, String key) {
		SharedPreferences prefs = getPreferences(context);
		int value = prefs.getInt(key, 0);
		return value;
	}

	public static void putPrefStrings(Context context, Map<String, String> map) {
		SharedPreferences prefs = getPreferences(context);
		SharedPreferences.Editor editor = prefs.edit();
		Iterator<Entry<String, String>> iterator = map.entrySet().iterator();
		while (iterator.hasNext()) {
			Map.Entry<String, String> pair = (Map.Entry<String, String>) iterator.next();
			editor.putString((String) pair.getKey(), (String) pair.getValue());
		}
		editor.commit();
	}

	public static void putPrefBooleans(Context context, Map<String, Boolean> map) {
		SharedPreferences prefs = getPreferences(context);
		SharedPreferences.Editor editor = prefs.edit();
		Iterator<Entry<String, Boolean>> iterator = map.entrySet().iterator();
		while (iterator.hasNext()) {
			Map.Entry<String, Boolean> pair = (Map.Entry<String, Boolean>) iterator.next();
			editor.putBoolean((String) pair.getKey(), (Boolean) pair.getValue());
		}
		editor.commit();
	}	
	
	public static int getAppVersion(Context context) {
		try {
			PackageInfo packageInfo = context.getPackageManager()
					.getPackageInfo(context.getPackageName(), 0);
			return packageInfo.versionCode;
		} catch (NameNotFoundException e) {
			throw new RuntimeException("Could not get package name: " + e);
		}
	}

	public static boolean isNetworkAvailable(Context context) {
		ConnectivityManager connectivityManager = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager
				.getActiveNetworkInfo();
		return activeNetworkInfo != null;
	}

	public static String getDeviceName() {
		String manufacturer = Build.MANUFACTURER;
		String model = Build.MODEL;
		if (model.startsWith(manufacturer)) {
			return Capitalize(model);
		} else {
			return Capitalize(manufacturer) + " " + model;
		}
	}

	public static String Capitalize(String s) {
		if (s == null || s.length() == 0) {
			return "";
		}
		char first = s.charAt(0);
		if (Character.isUpperCase(first)) {
			return s;
		} else {
			return Character.toUpperCase(first) + s.substring(1);
		}
	}
	
	public static void sendNotification(Context context, Bundle bundle) {
		String msg = CommandUtils.getMessage(bundle);
		NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		PendingIntent pendingIntent = PendingIntent.getActivity(context, 
				0, new Intent(context, RemoteHome.class), 0);
		NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
			.setSmallIcon(R.drawable.ic_notification)
			.setContentTitle("Command Received")
			.setStyle(new NotificationCompat.BigTextStyle()
			.bigText(msg))
			.setContentText(msg);
		builder.setContentIntent(pendingIntent);
		manager.notify(Constants.NOTIFICATION_ID, builder.build());
	}
	
	public static void notification(Context context) {
		Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		Ringtone ringtone = RingtoneManager.getRingtone(context, notification);
		ringtone.play();
	}
	
	public static int buildVersion() {
		return android.os.Build.VERSION.SDK_INT;
	}
	
	public static int getDisplayDensity(Context context) {
		DisplayMetrics metrics = new DisplayMetrics();
		((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(metrics);
		return metrics.densityDpi;
	}
	
	public static int getDisplaySize(Context context, int scaled) {
		int density = Utils.getDisplayDensity(context); 
		int size;
		switch (density) {
		case Constants.DENSITY_LOW:
			size = 32;
			break;
		case Constants.DENSITY_MEDIUM:
			size = 48;
			break;
		case Constants.DENSITY_HIGH:
			size = 72;
			break;
		case Constants.DENSITY_XHIGH:
			size = 96;
			break;
		case Constants.DENSITY_XXHIGH:
			size = 144;
			break;
		default:
			size = 72;
			break;
		}
		
		switch (scaled) {
		case Constants.DENSITY_UNSCALED:
			return size;
		case Constants.DENSITY_SCALED:
			return (int)(size * 1.5);
		default:
			return size;
		}
	}
}
