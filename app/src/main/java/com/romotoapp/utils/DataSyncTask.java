package com.romotoapp.utils;

import org.json.JSONObject;

import com.romotoapp.interfaces.OnDataSyncListener;

import android.os.AsyncTask;

public class DataSyncTask extends AsyncTask<String, Void, Void> {
	private JSONParser jsonParser;
	private JSONObject jsonObject;
	private OnDataSyncListener listener;

	public DataSyncTask(OnDataSyncListener listener) {
		this.listener = listener;
	}

	public DataSyncTask() {
		
	}
	
	@Override
	protected Void doInBackground(String... params) {
		jsonParser = new JSONParser();
		jsonObject = jsonParser.getJSONFromUrl(params[0]);
		return null;
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		listener.onSyncComplete(jsonObject);
	}
}