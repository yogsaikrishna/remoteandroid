package com.romotoapp.utils;

import java.io.IOException;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.romotoapp.constants.Constants;

public class GCMUtils {
	private static final String TAG = "GCMUtils";
	
	public static boolean checkPlayServices(Activity context) {
		int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
		if (resultCode != ConnectionResult.SUCCESS) {
			if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
				GooglePlayServicesUtil.getErrorDialog(resultCode, context, 
						Constants.PLAY_SERVICES_RESOLUTION_REQUEST).show();
			} else {
				Log.i(TAG, "This device is not supported!");
				context.finish();
			}
			return false;
		}
		return true;
	}
	
	public static String getRegistrationId(Context context) {
		SharedPreferences prefs = Utils.getPreferences(context);
		String registrationId = prefs.getString(Constants.PROPERTY_REG_ID, null);
		if (registrationId == null) {
			Log.i(TAG, "Registration not found");
			return null;
		}
		int registeredVersion = prefs.getInt(Constants.PROPERTY_APP_VERSION, Integer.MIN_VALUE);
		int currentVersion = Utils.getAppVersion(context);
		if (registeredVersion != currentVersion) {
			Log.i(TAG, "App version changed");
			return null;
		}
		return registrationId;
	}
	
	public static void storeRegistrationId(Context context, String regId) {
		SharedPreferences prefs = Utils.getPreferences(context);
		int appVersion = Utils.getAppVersion(context);
		Log.i(TAG, "Saving regId on app version " + appVersion);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString(Constants.PROPERTY_REG_ID, regId);
		editor.putInt(Constants.PROPERTY_APP_VERSION, appVersion);
		editor.commit();
	}
	
	public static void register(final Context context) {
		new AsyncTask<Void, Void, String>() {
			@Override
			protected String doInBackground(Void... params) {
				String message = null;
				String regId = null;
				try {
					GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(context);
					while (regId == null) {
						regId = gcm.register(Constants.SENDER_ID);
					}
					message = "Device registered, registration Id: " + regId;
					storeRegistrationId(context, regId);
				} catch (IOException e) {
					message = "Error : " + e.getMessage();
				}
				return message;
			}

			@Override
			protected void onPostExecute(String message) {
				super.onPostExecute(message);
				Log.i(TAG, message);
			}
		}.execute(null, null, null);
	}
}
