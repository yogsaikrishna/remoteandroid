package com.romotoapp;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.romotoapp.constants.Constants;
import com.romotoapp.interfaces.OnDataSyncListener;
import com.romotoapp.utils.DataSyncTask;
import com.romotoapp.utils.Utils;

public class Registration extends BaseActivity implements OnDataSyncListener {
	private static final String TAG = "Registration";
	
	private EditText etName, etEmail, etMobile;
	private Button btRegister, btClear;
	private String name, email, mobile;
	private DataSyncTask dataSyncTask;
	private ProgressDialog progressDialog;
	private String gcmId;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getSupportActionBar().hide();
		setContentView(R.layout.registration);
		
		etName = (EditText) findViewById(R.id.et_name);
		etEmail = (EditText) findViewById(R.id.et_email);
		etMobile = (EditText) findViewById(R.id.et_mobile);
		btRegister = (Button) findViewById(R.id.bt_register);
		btClear = (Button) findViewById(R.id.bt_clear);
		
		dataSyncTask = new DataSyncTask(this);
		email = Utils.getUserEmail(Registration.this);
		
		if (email != null) {
			etEmail.setText(email);
		}
		
		btRegister.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				name = etName.getText().toString();
				email = etEmail.getText().toString();
				mobile = etMobile.getText().toString();
				
				if (name == null 
						|| name.equals("")) {
					Utils.makeToast(getApplicationContext(), Constants.WARN_NAME, Toast.LENGTH_SHORT);
				} else if (email == null 
						|| email.equals("")) {
					Utils.makeToast(getApplicationContext(), Constants.WARN_EMAIL, Toast.LENGTH_SHORT);
				} else if (mobile == null 
						|| mobile.equals("")) {
						Utils.makeToast(getApplicationContext(), Constants.WARN_MOBILE, Toast.LENGTH_SHORT);
				} else if (mobile.length() < 10) {
						Utils.makeToast(getApplicationContext(), Constants.WARN_INVALID_MOBILE, Toast.LENGTH_SHORT);
				} else {
					if (Utils.isNetworkAvailable(Registration.this)) {
						gcmId = Utils.getPrefString(Registration.this, Constants.PROPERTY_REG_ID);
						dataSyncTask.execute(Constants.URL_REGISTRATION
								+ "name=" + Utils.Capitalize(name).replace(" ", "+")
								+ "&gcm_id=" + gcmId
								+ "&email=" + email
								+ "&mobile=" + mobile
								+ "&model=" + Utils.getDeviceName().replace(" ", "+"));
						progressDialog = ProgressDialog.show(Registration.this, "", "Please wait...");
						Map<String, String> tmpMap = new HashMap<String, String>();
						tmpMap.put(Constants.PROPERTY_NAME, Utils.Capitalize(name));
						tmpMap.put(Constants.PROPERTY_EMAIL, email);
						tmpMap.put(Constants.PROPERTY_MOBILE_NO, mobile);
						Utils.putPrefStrings(Registration.this, tmpMap);
					} else {
						Log.i(TAG, Constants.WARN_NO_NETWORK);
						Utils.makeToast(getApplicationContext(), Constants.WARN_NO_NETWORK, Toast.LENGTH_SHORT);
					}
				}
			}
		});
		
		btClear.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				etName.setText("");
				etMobile.setText("");
			}
		});
	}

	@Override
	public void onSyncComplete(JSONObject jsonObj) {
		try {
			String status = jsonObj.getString(Constants.PROPERTY_STATUS);
			if (status.equals("OK")) {
				String id = jsonObj.getString(Constants.PROPERTY_ID);
				String name = jsonObj.getString(Constants.PROPERTY_NAME);
				String userKey = jsonObj.getString(Constants.PROPERTY_USER_KEY);
				String model = jsonObj.getString(Constants.PROPERTY_MODEL);
				Map<String, String> map = new HashMap<String, String>();
				map.put(Constants.PROPERTY_ID, id);
				map.put(Constants.PROPERTY_NAME, name);
				map.put(Constants.PROPERTY_USER_KEY, userKey);
				map.put(Constants.PROPERTY_MODEL, model);
				Utils.putPrefStrings(Registration.this, map);
				progressDialog.dismiss();
				Intent intent = new Intent(Registration.this, RemoteHome.class);
				startActivity(intent);
				Map<String, Boolean> hashMap = new HashMap<String, Boolean>();
				hashMap.put(Constants.PROPERTY_FIRST_RUN, true);
				Utils.putPrefBooleans(Registration.this, hashMap);
				finish();
			} else {
				Utils.makeToast(getApplicationContext(), Constants.ERR_TRY_AGAIN, Toast.LENGTH_SHORT);
				progressDialog.dismiss();
			}
		} catch (JSONException e) {
			Log.i(TAG, "Exception occurred: " + e.getMessage());
		}
	}
	
}
